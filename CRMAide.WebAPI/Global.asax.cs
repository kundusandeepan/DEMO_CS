﻿using CRMAide.WebAPI.App_Start;
using System;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CRMAide.BusinessEntities;
using CRMAide.WebAPI.Controllers;

namespace CRMAide.WebAPI
{
    public class Global : HttpApplication
    {
        private void Application_Start(object sender, EventArgs e)
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            AreaRegistration.RegisterAllAreas();
            //RouteConfig.RegisterRoutes(RouteTable.Routes);
            AutoMapperConfiguration.Configure();
            
            //ModelBinders.Binders[typeof (ContactServiceDTO)] = new CustomBinder<ContactServiceDTO>();

        }
    }
}