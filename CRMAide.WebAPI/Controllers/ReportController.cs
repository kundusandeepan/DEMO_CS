﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime;
using System.Web;
using System.Web.Http;
using CRMAide.BusinessEntities;
using CRMAide.Data.Interface;
using CRMAide.Data.Model;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Newtonsoft.Json;

namespace CRMAide.WebAPI.Controllers
{
    public class ReportController : ApiController
    {
        private readonly IReportService _reportservice;
        private readonly IReportCategoryService _reportCategoryService;
        private readonly IReportParameterService _reportParameterService;
        public ReportController(IReportService reportservice,IReportCategoryService reportCategoryService, IReportParameterService reportParameterService)
        {
            _reportservice = reportservice;
            _reportCategoryService = reportCategoryService;
            _reportParameterService = reportParameterService;
        }

        [Route("api/report/tree")]
        public IEnumerable<TreeNode> GetReportTree()
        {
            return BuildTree(0);
        }

        [Route("api/report/parameters")]
        public IEnumerable<ReportParameterMetaData> GetReportParameter(int reportid)
        {
            var outputList = new List<ReportParameterMetaData>();
            var dataList = _reportParameterService.FilterBy(t => t.Report.ReportId == reportid);
            foreach (var data in dataList)
            {
                var updatedData = AutoMapper.Mapper.Map<ReportParameterMetaData>(data);
                if (string.IsNullOrEmpty(data.SourceOptions) && !string.IsNullOrEmpty(data.SourceSP))
                {
                    var listoptions = _reportParameterService.GetSourceSPData(data.SourceSP);
                    updatedData.ListOptions2 = AutoMapper.Mapper.Map<List<BusinessEntities.ListOptions>>(listoptions);
                }
                outputList.Add(updatedData);
            }

            return outputList;
        }


        #region "Build Tree"
        private IEnumerable<TreeNode> BuildTree(int startReportCategoryId)
        {
            var nodeList = new List<TreeNode>();
            foreach (var category in GetReportCategoryByParentId(startReportCategoryId))
            {
                var tn = new TreeNode
                {
                    Name = category.ReportCategoryName,
                    Id = category.ReportCategoryId,
                };
                foreach (var report in GetReportByCategoryId(category.ReportCategoryId))
                {
                    var report_tn = new ReportTreeNode
                    {
                        Id = report.ReportId,
                        Name = report.ReportName,
                        Description = report.ReportDescription,
                        IsGraph = report.ReportIsGraph
                    };
                    tn.Children.Add(report_tn);
                }
                if (category.ReportCategoryId > 0)
                    tn.Children.AddRange(BuildTree(category.ReportCategoryId));
                nodeList.Add(tn);
            }
            return nodeList;
        }

        private IEnumerable<Report> GetReportByCategoryId(int categoryid)
        {
            return _reportservice.FilterBy(t => t.ReportCategory.ReportCategoryId == categoryid).ToList();
        }

        private IEnumerable<ReportCategory> GetReportCategoryByParentId(int parentId)
        {
            return _reportCategoryService.FilterBy(t => t.ReportCategoryParentId == parentId).ToList();
        }
        #endregion
        
        [HttpPost]
        [Route("api/report/report")]
        public string GenerateReport(ReportInputDTO input)
        {
            var dt = _reportservice.GetTabularReport(input.ReportId, input.FieldValuePair);
            if (dt != null)
            {
                string json = JsonConvert.SerializeObject(dt, Formatting.Indented);
                return json;
            }
            return string.Empty;
        }
        

        [HttpPost]
        [Route("api/report/graph")]
        public string GenerateGraph(ReportInputDTO input)
        {
            return "hello - graph";
        }
        
    }

    public class ReportInputDTO
    {
        public virtual int ReportId { get; set; }
        public Dictionary<int, string> FieldValuePair { get; set; }
    }
    
}