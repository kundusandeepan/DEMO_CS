﻿using System;
using CRMAide.Data.Interface;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BE = CRMAide.BusinessEntities;

namespace CRMAide.WebAPI.Controllers
{
    public class ContactServiceDataController : ApiController
    {
        private readonly IContactServiceDataService _contactServiceDataService;
        private readonly IContactService _contactservice;

        public ContactServiceDataController(IContactServiceDataService contactservicedataservice, IContactService contactservice)
        {
            _contactServiceDataService = contactservicedataservice;
            _contactservice = contactservice;
        }
        

        /// <summary>
        /// Creates a new Contact->Service Record
        /// </summary>
        public int Post(int contactid, int serviceid, string description)
        {
            try
            {
                //TODO: Validation
                return _contactServiceDataService.AddService(contactid, serviceid, description);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    ReasonPhrase = "Error: " + ex.Message
                });
            }
        }

        public BE.ContactService GetService(int contactserviceid)
        {
            var result = _contactServiceDataService.FindBy(t => t.ContactServiceID == contactserviceid);
            if (result == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    ReasonPhrase = "Cannot find Contact by the mentioned ID. Contact Administrator."
                });
            }
            var response = AutoMapper.Mapper.Map<BE.ContactService>(result);
            return response;
        }

        public BE.ContactService Put(BE.ContactServiceDTO inputDTO)
        {
            try
            {
                //TODO: Validation
                var input = _contactServiceDataService.FindBy(t => t.ContactServiceID == inputDTO.SubscriptionId);

                foreach (var valuePair in inputDTO.FieldValuePair)
                {
                    var item = input.ContactServiceField.FirstOrDefault(t => t.ServiceField.ServiceFieldID == valuePair.Key);
                    if (item != null)
                    {
                        item.ServiceFieldValue = valuePair.Value;
                    }
                }
                if (_contactServiceDataService.Update(input))
                {
                    var response = AutoMapper.Mapper.Map<BE.ContactService>(input);
                    return response;
                }
                else
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                    {
                        ReasonPhrase = "Could not be saved. Contact Administrator."
                    });
                }
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    ReasonPhrase = "Error: " + ex.Message
                });
            }
        }


        /// <summary>
        /// Delete Record by contactserviceid
        /// </summary>
        /// <param name="contactserviceid">id of contactservice to be deleted</param>
        public void Delete(int contactserviceid)
        {
            try
            {
                if (!_contactservice.DeleteSubscription(contactserviceid))
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                    {
                        ReasonPhrase = "Could not be deleted. Contact Administrator."
                    });
                }
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    ReasonPhrase = "Error: " + ex.Message
                });
            }
        }
        
    }
}