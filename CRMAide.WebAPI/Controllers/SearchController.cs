﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using CRMAide.Data.Interface;
using CRMAide.Data.Model;

namespace CRMAide.WebAPI.Controllers
{
    public class SearchController : ApiController
    {
        private readonly IContactService _contactService;
        public SearchController(IContactService contactService)
        {
            _contactService = contactService;
        }
        [Route("api/Search")]
        public IList<ContactSearchResult> GetSearchContacts(string query)
        {
            return _contactService.Search(query);//.Take(8).ToList();
        }
    }
}