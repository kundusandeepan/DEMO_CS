﻿using CRMAide.BusinessEntities;
using CRMAide.Data.Interface;
using System.Collections.Generic;
using System.Web.Http.Description;

namespace CRMAide.WebAPI.Controllers
{
    public class TestController : BaseApiController
    {
        public TestController(IAuthenticationService service)
        {
        }

        public IEnumerable<TestOutputDTO> Get()
        {
            return null;
        }

        [ApiExplorerSettings(IgnoreApi = true)] //this attibute hides the api
        public bool Post(TestInputDTO input)
        {
            return true;
        }
    }
}