﻿using AutoMapper;
using CRMAide.BusinessEntities;
using CRMAide.Data.Interface;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Address = CRMAide.Data.Model.Address;

namespace CRMAide.WebAPI.Controllers
{
    public class HouseholdController : ApiController
    {
        private readonly IAddressService _addresservice;
        private readonly IMasterDataService _masterDataServive;
        
        public HouseholdController(IAddressService addresservice,IMasterDataService masterDataServive)
        {
            _addresservice = addresservice;
            _masterDataServive = masterDataServive;
        }
        

        /// <summary>
        /// List of all Addresses
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Address> Get()
        {
            //var allcountries = _masterDataServive.GetCountries();
            //var allstates = _masterDataServive.GetStates();
            return
                _addresservice.All()
                    //.OrderBy(a => allcountries.Where(country => country.CountryID == a.CountryID))
                    //.ThenBy(b => allstates.Where(state=> state.CountryID == b.CountryID &&  state.StateID == b.StateID))
                    .OrderBy(a =>a.CountryID)
                    .ThenBy(b => b.StateID)
                    .ThenBy(c => c.City)
                    .ThenBy(d => d.Street1);
        }

        [Route("api/Household/GetCities")]
        public IEnumerable<string> PostCities(CitiesAutoCompleteDTO input)
        {
            return _addresservice.GetCities(input.Query, input.StateId, input.CountryId);
            //t.City.StartsWith(input.Query) && 
            //var d1 = _addresservice.FilterBy(t => t.City.Contains(input.Query) &&  t.StateID == input.StateId && t.CountryID == input.CountryId);
            //var d2 = d1.OrderBy(t => t.City);
            //var d3 = d2.Distinct().ToList();
            //    return d3.ConvertAll<string>(t=>t.City);
        }

        /// <summary>
        /// Address detail of provided AddressId
        /// </summary>
        /// <param name="id">Address id</param>
        /// <returns></returns>
        public Address Get(int id)
        {
            return _addresservice.FindBy(t => t.AddressID == id);
        }

        /// <summary>
        /// Creates a new Address Record
        /// </summary>
        /// <param name="input">Address Details to be inserted</param>
        /// <returns></returns>
        public Address Post(AddressInputDTO input)
        {
            try
            {
                //TODO: Validation
                var inputrecord = Mapper.Map<Address>(input);

                if (_addresservice.Add(inputrecord))
                {
                    return inputrecord;
                }
                else
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                    {
                        ReasonPhrase = "Could not be saved. Contact Administrator."
                    });
                }
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    ReasonPhrase = "Error: " + ex.Message
                });
            }
            //throw new HttpResponseException("Some error message", HttpStatusCode.BadRequest);
        }

        /// <summary>
        /// updated address record by AddressId
        /// </summary>
        /// <param name="input">Address details to be updated</param>
        /// <returns></returns>
        public Address Put(AddressDTO input)
        {
            try
            {
                //TODO: Validation
                var inputrecord = Mapper.Map<Address>(input);
                if (_addresservice.Update(inputrecord))
                {
                    return inputrecord;
                }
                else
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                    {
                        ReasonPhrase = "Could not be saved. Contact Administrator."
                    });
                }
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    ReasonPhrase = "Error: " + ex.Message
                });
            }
            //throw new HttpResponseException("Some error message", HttpStatusCode.BadRequest);
        }

        /// <summary>
        /// Delete Record by Address Id
        /// </summary>
        /// <param name="addressid">id of address to be deleted</param>
        public void Delete(int addressid)
        {
            try
            {
                if (!_addresservice.Delete(t => t.AddressID == addressid))
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                    {
                        ReasonPhrase = "Could not be deleted. Contact Administrator."
                    });
                }
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    ReasonPhrase = "Error: " + ex.Message
                });
            }
        }

        /// <summary>
        /// Search By various Address Parameters
        /// </summary>
        /// <param name="search">Search input conditions</param>
        /// <returns></returns>
        [Route("api/Household/Search")]
        [HttpPost]
        public IEnumerable<Address> Search(AddressSearchDTO search)
        {
            var result = Mapper.Map<Address>(search);
            return _addresservice.Search(result);
        }
    }
}