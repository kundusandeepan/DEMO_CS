﻿using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Web.Http;

namespace CRMAide.WebAPI.Controllers
{
    public class BaseApiController : ApiController
    {
        protected string GetReferrerUrl()
        {
            return Request.Headers.Referrer == null ? this.Request.RequestUri.GetLeftPart(System.UriPartial.Authority) : Request.Headers.Referrer.ToString();
        }

        protected int GetUserId()
        {
            var userid = -1;
            var useridentity = User.Identity as GenericIdentity;
            var useridclaim = useridentity?.Claims.FirstOrDefault(t => t.Type == ClaimTypes.Sid);
            if (useridclaim != null)
            {
                int.TryParse(useridclaim.Value, out userid);
            }
            return userid;
        }
    }
}