﻿using CRMAide.Data.Interface;
using CRMAide.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using AutoMapper;

namespace CRMAide.WebAPI.Controllers
{
    public class ContactController : BaseApiController
    {
        private readonly IContactService _contactservice;
        private readonly IRecentContactService _recentContactService;
        private readonly IAddressService _addressService;
        
        public ContactController(IContactService contactservice, IRecentContactService recentContactService, IAddressService addressService)
        {
            _contactservice = contactservice;
            _recentContactService = recentContactService;
            _addressService = addressService;
           

        }

        [Route("api/Contact/GetContactsByAddressId")]
        public IEnumerable<CRMAide.BusinessEntities.Contact> GetContactsByAddressId(int addressId)
        {
            var result = _contactservice.FilterBy(t => t.AddressID == addressId);
            var response = AutoMapper.Mapper.Map<IEnumerable<CRMAide.BusinessEntities.Contact>>(result);
            return response;
        }

        [Route("api/Contact/GetAllContacts")]
        public IEnumerable<CRMAide.BusinessEntities.ContactShortDTO> GetAllContacts()
        {
            var result = _contactservice.All();//.Take(10);
            var response = AutoMapper.Mapper.Map<IEnumerable<CRMAide.BusinessEntities.ContactShortDTO>>(result);
            return response;
        }
        
        [Route("api/Contact")]
        public CRMAide.BusinessEntities.Contact GetContact(int contactId)
        {
            var result = _contactservice.FindBy(t => t.ContactID == contactId);
            var response = AutoMapper.Mapper.Map<BusinessEntities.Contact>(result);
            if (response != null)
            {
                var userid = GetUserId();
                if (userid > -1)
                {
                    _recentContactService.Add(new RecentContact()
                    {
                        ContactId = response.ContactID,
                        AddressID = response.AddressID,
                        ContactName = GetFormattedName(response),
                        LoggedUserId = userid,
                        AccessDatetime = DateTime.Now
                    });
                }
            }
            return response;
        }

        private string GetFormattedName(BusinessEntities.Contact contact)
        {
            var sb = new StringBuilder();
            if (contact != null)
            {
                if (!string.IsNullOrEmpty(contact.FirstName))
                    sb.Append(contact.FirstName);

                if (!string.IsNullOrEmpty(contact.MiddleName))
                {
                    if (sb.Length > 0)
                        sb.Append(" ");
                    sb.Append(contact.MiddleName);
                }

                if (!string.IsNullOrEmpty(contact.LastName))
                {
                    if (sb.Length > 0)
                        sb.Append(" ");
                    sb.Append(contact.LastName);
                }
            }
            string response = sb.ToString();
            return response;
        }

        /// <summary>
        /// updated Contact record by ContactId
        /// </summary>
        /// <param name="input">Contact details to be updated</param>
        /// <returns></returns>
        [Route("api/Contact")]
        public Contact Put(BusinessEntities.Contact inputDTO)
        {
            try
            {
                //TODO: Validation
                var contact = _contactservice.FindBy(t => t.ContactID == inputDTO.ContactID);
               
                var input = Mapper.Map(inputDTO, contact);
                input.ServicesSubscription = contact.ServicesSubscription;
                input.Notes = contact.Notes;
                if (_contactservice.Update(input))
                {
                    return input;
                }
                else
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                    {
                        ReasonPhrase = "Could not be saved. Contact Administrator."
                    });
                }
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    ReasonPhrase = "Error: " + ex.Message
                });
            }
            //throw new HttpResponseException("Some error message", HttpStatusCode.BadRequest);
        }

        [Route("api/Contact")]
        public Contact Post(BusinessEntities.Contact inputDTO)
        {
            try
            {
                //TODO: Validation
                var input = Mapper.Map<Contact>(inputDTO);
                if (_contactservice.Add(input) )
                {
                    return input;
                }
                else
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                    {
                        ReasonPhrase = "Could not be saved. Contact Administrator."
                    });
                }
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    ReasonPhrase = "Error: " + ex.Message
                });
            }
            //throw new HttpResponseException("Some error message", HttpStatusCode.BadRequest);
        }


        /// <summary>
        /// Delete Record by ContactId
        /// </summary>
        /// <param name="contactid">id of Contact </param>
        [Route("api/Contact")]
        public void Delete(int contactid)
        {
            try
            {
                if (!_contactservice.Delete( t=>t.ContactID == contactid))
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                    {
                        ReasonPhrase = "Could not be deleted. Contact Administrator."
                    });
                }
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    ReasonPhrase = "Error: " + ex.Message
                });
            }
        }
    }
}