﻿using System.Collections.Generic;
using System.Linq;
using CRMAide.Data.Interface;
using System.Web.Http;
using CRMAide.Data.Model;

namespace CRMAide.WebAPI.Controllers
{
    public class MasterDataController : ApiController
    {
        private readonly IMasterDataService _masterdataservice;

        public MasterDataController(IMasterDataService masterdataservice)
        {
            _masterdataservice = masterdataservice;
        }

        /// <summary>
        /// List of all Countries
        /// </summary>
        /// <returns></returns>
        [Route("api/MasterData/Countries")]
        public IList<Country> GetCountries()
        {
            return _masterdataservice.GetCountries().ToList();
        }
        /// <summary>
        /// List of all States
        /// </summary>
        /// <returns></returns>
        [Route("api/MasterData/States")]
        public IList<State> GetStates()
        {
            return _masterdataservice.GetStates().ToList();
        }
        /// <summary>
        /// List of all Salutation
        /// </summary>
        /// <returns></returns>
        [Route("api/MasterData/Salutation")]
        public IList<SalutationType> GetSalutation()
        {
            return _masterdataservice.GetSalutations().ToList();
        }


        /// <summary>
        /// List of all GetServiceOptions
        /// </summary>
        /// <returns></returns>
        [Route("api/MasterData/GetServiceOptions")]
        public IEnumerable<ServiceOption> GetServiceOptions()
        {
            return _masterdataservice.GetServiceOptions();
        }

        /// <summary>
        /// List of all RelationTypes
        /// </summary>
        /// <returns></returns>
        [Route("api/MasterData/ContactTypes")]
        public IEnumerable<ContactType> GetContactTypes()
        {
            return _masterdataservice.GetContactTypes();
        }

    }
}