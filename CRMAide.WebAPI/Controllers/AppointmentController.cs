﻿using System;
using CRMAide.Data.Interface;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using CRMAide.BusinessEntities;
using CRMAide.Data.Model;

namespace CRMAide.WebAPI.Controllers
{
    public class AppointmentController : BaseApiController
    {
        private readonly IAppointmentService _appointmentService;
        private readonly IAideUserService _userService;
        private int _loggedinUserId;
        public AppointmentController(IAppointmentService appointmentService, IAideUserService userService)
        {
            _appointmentService = appointmentService;
            _userService = userService;
            _loggedinUserId = GetUserId();

            Mapper.CreateMap<AppointmentInputDTO, Data.Model.Appointment>()
                .ForMember(dest => dest.StartAt,
                    optns => optns.MapFrom(src => src.Start.ToUniversalTime()))
                .ForMember(dest => dest.EndAt,
                    optns => optns.MapFrom(src => src.End.ToUniversalTime()))
                .ForMember(dest => dest.User,
                    optns => optns.MapFrom(src => _userService.FindBy(t => t.UserID == _loggedinUserId)));

        }

        /// <summary>
        /// List of all Appointments
        /// </summary>
        /// <returns></returns>
        public IEnumerable<BusinessEntities.Appointment> Get()
        {
            var xx = _appointmentService.FilterBy(t => t.User.UserID == _loggedinUserId);
            var yy = AutoMapper.Mapper.Map<IEnumerable<BusinessEntities.Appointment>>(xx);
            return yy;
        }

        /// <summary>
        /// Creates a new Appointment Record
        /// </summary>
        /// <param name="input">Appointment Details to be inserted</param>
        /// <returns></returns>
        public BusinessEntities.Appointment Post(AppointmentInputDTO input)
        {
            try
            {
                //TODO: Validation
                var appointment = Mapper.Map<Data.Model.Appointment>(input);
                
                appointment.User = _userService.FindBy(t => t.UserID == _loggedinUserId);

                appointment.Title = input.Title;
                appointment.Description = input.Description;
//                appointment.StartAt = input.Start;
//                appointment.EndAt = input.End;
                appointment.IsFullDay = input.IsFullDay;

                if (_appointmentService.Add(appointment))
                {
                    return Mapper.Map<BusinessEntities.Appointment>(appointment);
                }
                else
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                    {
                        ReasonPhrase = "Could not be saved. Contact Administrator."
                    });
                }
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    ReasonPhrase = "Error: " + ex.Message
                });
            }
        }

        /// <summary>
        /// Updates an appointment
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public BusinessEntities.Appointment Put(AppointmentInputDTO input)
        {
            try
            {
                //TODO: Validation
                var appointment = _appointmentService.FindBy(t => t.Id == input.Id);
                
                appointment.Title = input.Title;
                appointment.Description = input.Description;
                appointment.StartAt = input.Start;
                appointment.EndAt = input.End;
                appointment.IsFullDay = input.IsFullDay;

                if (_appointmentService.Update(appointment))
                {
                    return Mapper.Map<BusinessEntities.Appointment>(appointment);
                }
                else
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                    {
                        ReasonPhrase = "Could not be saved. Contact Administrator."
                    });
                }
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    ReasonPhrase = "Error: " + ex.Message
                });
            }
        }

        /// <summary>
        /// Deletes an appointment by id.
        /// </summary>
        /// <param name="id"></param>
        public void Delete(int id)
        {
            try
            {
                if (!_appointmentService.Delete(t => t.Id== id ))
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                    {
                        ReasonPhrase = "Could not be deleted. Contact Administrator."
                    });
                }
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    ReasonPhrase = "Error: " + ex.Message
                });
            }
        }
    }
}