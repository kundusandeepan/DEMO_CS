﻿using CRMAide.Data.Interface;
using CRMAide.Data.Model;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace CRMAide.WebAPI.Controllers
{
    public class RecentController : BaseApiController
    {
        private readonly IRecentContactService _recentContactService;

        public RecentController(IRecentContactService recentContactService)
        {
            _recentContactService = recentContactService;
        }

        [Route("api/recent/contacts")]
        public IEnumerable<RecentContact> GetRecentContacts()
        {
            var userid = GetUserId();
            if (userid > -1)
            {
                return _recentContactService.FilterBy(t => t.LoggedUserId == userid)
                    .OrderByDescending(t => t.AccessDatetime)
                    .GroupBy(
                        key => key.ContactId,
                        (key, element) => element.FirstOrDefault()
                    ).Take(10);
            }

            return null;
        }
    }
}