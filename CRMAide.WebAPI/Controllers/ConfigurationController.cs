﻿using CRMAide.BusinessEntities;
using System.Collections.Generic;
using System.Web.Http;

namespace CRMAide.WebAPI.Controllers
{
    public class ConfigurationController : ApiController
    {
        /// <summary>
        /// List all Menus
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Menu> GetAllMenus()
        {
            var menus = new List<Menu>();

            menus.Add(new Menu()
            {
                DisplayName = "Main",
                Id = 1,
                SubMenus = new List<Menu>()
                {
                    new Menu() {DisplayName = "Dashboard" },
                    new Menu() {DisplayName = "Profile" },
                    new Menu() {DisplayName = "SiteMap" },
                    new Menu() {DisplayName = "Search" }
                }
            });
            menus.Add(new Menu()
            {
                DisplayName = "Client",
                Id = 1,
                SubMenus = new List<Menu>()
                {
                    new Menu() {DisplayName = "Household" },
                    new Menu() {DisplayName = "Contact" },
                    new Menu() {DisplayName = "Search" }
                }
            });

            return menus;
        }
    }
}