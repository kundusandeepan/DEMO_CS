﻿using AutoMapper;
using CRMAide.Data.Interface;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CRMAide.BusinessEntities;

namespace CRMAide.WebAPI.Controllers
{
    public class AssociationController : ApiController
    {
        private readonly IAssociationService _associationService;
        private readonly IContactService _contactService;
        private readonly IMasterDataService _masterDataService;

        public AssociationController(IAssociationService associationService, IContactService contactService, IMasterDataService masterDataService)
        {
            _associationService = associationService;
            _contactService = contactService;
            _masterDataService = masterDataService;

            Mapper.CreateMap<AssociationDTO, Data.Model.Association>()
                .ForMember(dest => dest.PrimaryContact,
                    optns => optns.MapFrom(src => contactService.FindBy(t => t.ContactID == src.PrimaryContactID)))
                .ForMember(dest => dest.SecondayContact,
                    optns => optns.MapFrom(src => contactService.FindBy(t => t.ContactID == src.RelationContactID)))
                .ForMember(dest => dest.ContactType,
                    optns => optns.MapFrom(src => masterDataService.GetContactTypes().FirstOrDefault(t => t.ContactTypeID == src.ContactTypeID)))
                .ForMember(dest => dest.IsAssociateDependent,
                    optns => optns.MapFrom(src =>src.IsAssociateDependent));

        }
        
        public Association Post(AssociationDTO inputDTO)
        {
            try
            {
                //TODO: Validation
                var input = Mapper.Map<Data.Model.Association>(inputDTO);
                if (_associationService.Add(input))
                {
                    return Mapper.Map<Association>(input);
                }
                else
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                    {
                        ReasonPhrase = "Could not be saved. Contact Administrator."
                    });
                }
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    ReasonPhrase = "Error: " + ex.Message
                });
            }
        }

        public void Delete(int PrimaryContactID, int RelationContactID)
        {
            try
            {

                var association = _associationService.FindBy(t => t.PrimaryContact.ContactID == PrimaryContactID
                                                                  && t.SecondayContact.ContactID == RelationContactID);
                if (association !=null && !_associationService.Delete(association))
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                    {
                        ReasonPhrase = "Could not be deleted. Contact Administrator."
                    });
                }
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    ReasonPhrase = "Error: " + ex.Message
                });
            }
        }
    }
}