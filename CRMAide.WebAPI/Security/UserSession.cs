﻿using System.Security.Claims;

namespace CRMAide.WebAPI.Security
{
    public class UserSession : IUserSession
    {
        #region Constructor

        public UserSession(ClaimsPrincipal principal)
        {
            UserId = int.Parse(principal.FindFirst(ClaimTypes.Sid).Value);
            SessionToken = principal.FindFirst(ClaimTypes.PrimarySid).Value;
            Username = principal.FindFirst(ClaimTypes.Name).Value;
        }

        #endregion Constructor

        public int UserId { get; private set; }
        public string Username { get; private set; }
        public string SessionToken { get; private set; }
    }
}