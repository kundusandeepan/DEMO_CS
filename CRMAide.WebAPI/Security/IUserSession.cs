﻿namespace CRMAide.WebAPI.Security
{
    public interface IUserSession
    {
        int UserId { get; }
        string Username { get; }
        string SessionToken { get; }
    }
}