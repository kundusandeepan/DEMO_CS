﻿using CRMAide.BusinessServices;
using CRMAide.BusinessServices.Interface;
using Ninject.Modules;

namespace CRMAide.WebAPI.App_Start
{
    public class BusinessServiceModule : NinjectModule
    {
        public override void Load()
        {
            Bind<ITokenService>().To<TokenService>();
            //Bind<IUserService>().To<UserService>();
        }
    }
}