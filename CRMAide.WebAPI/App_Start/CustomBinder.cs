using System;
using System.Linq;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace CRMAide.WebAPI
{

    public class CustomBinder<T> : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            T model;

            if (controllerContext.RequestContext.HttpContext.Request.AcceptTypes.Contains("application/json", StringComparer.OrdinalIgnoreCase))
            {
                var form = controllerContext.RequestContext.HttpContext.Request.Form.ToString();
                model = JsonConvert.DeserializeObject<T>(form);
            }
            else
            {
                model = (T)ModelBinders.Binders.DefaultBinder.BindModel(controllerContext, bindingContext);
            }

            return model;
        }
    }
}