﻿using System;
using System.Text;
using AutoMapper;
using CRMAide.BusinessEntities;
using CRMAide.Data.Model;

namespace CRMAide.WebAPI.App_Start
{
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {

            Mapper.CreateMap<Token, TokenEntity>().ReverseMap();

            Mapper.CreateMap<AddressInputDTO, Address>().ReverseMap();
            Mapper.CreateMap<AddressDTO, Address>().ReverseMap();

            Mapper.CreateMap<AddressSearchDTO, Address>();

            Mapper.CreateMap<Data.Model.Contact, BusinessEntities.Contact>()
                .ForMember(dest => dest.ServicesSubscription,
                    optns => optns.MapFrom(src => src.ServicesSubscription));

            Mapper.CreateMap<Data.Model.Contact, BusinessEntities.ContactShortDTO>();

            Mapper.CreateMap<BusinessEntities.Contact, Data.Model.Contact>()
                .ForMember(dest => dest.ServicesSubscription,
                    optns => optns.Ignore())
                .ForMember(dest => dest.Notes,
                    optns => optns.Ignore());
            
            Mapper.CreateMap<Data.Model.ContactService, ServicesSubscription>()
                .ForMember(dest => dest.ServiceID,
                    optns => optns.MapFrom(src => src.Service.ServiceID))
                .ForMember(dest => dest.ServiceName,
                    optns => optns.MapFrom(src => src.Service.ServiceName))
                .ForMember(dest => dest.CreateDate,
                    optns => optns.MapFrom(src => src.CreateDate))
                .ForMember(dest => dest.ContactServiceID,
                    optns => optns.MapFrom(src => src.ContactServiceID));
           

            Mapper.CreateMap<ContactNote, Note>()
                .ForMember(dest => dest.LoggedBy,
                    optns => optns.MapFrom(src => src.LoggedByUser !=null? src.LoggedByUser.Login :""));
            Mapper.CreateMap<Note, ContactNote>();

            Mapper.CreateMap<Data.Model.ContactServiceField, BusinessEntities.ContactServiceField>()
                .ForMember(dest => dest.ContactServiceID,
                    optns => optns.MapFrom(src => src.ContactService.ContactServiceID))
                .ForMember(dest => dest.ServiceFieldID,
                    optns => optns.MapFrom(src => src.ServiceField.ServiceFieldID))
                    ;

            Mapper.CreateMap<CRMAide.Data.Model.ContactService, CRMAide.BusinessEntities.ContactService>()
                .ForMember(dest => dest.ContactID,
                    optns => optns.MapFrom(src => src.Contact.ContactID))
                .ForMember(dest => dest.ServiceID,
                    optns => optns.MapFrom(src => src.Service.ServiceID))
                    ;

            Mapper.CreateMap<CRMAide.Data.Model.Association, CRMAide.BusinessEntities.Association>()
                .ForMember(dest => dest.ContactTypeID,
                    optns => optns.MapFrom(src => src.ContactType.ContactTypeID))
                .ForMember(dest => dest.PrimaryContactID,
                    optns => optns.MapFrom(src => src.PrimaryContact.ContactID))
                .ForMember(dest => dest.RelationContactID,
                    optns => optns.MapFrom(src => src.SecondayContact.ContactID))
                .ForMember(dest => dest.RelationAddressID,
                    optns => optns.MapFrom(src => src.SecondayContact.AddressID))
                .ForMember(dest => dest.RelationContactName,
                    optns => optns.MapFrom(src => GetFormattedName(src.SecondayContact)));

            Mapper.CreateMap<Data.Model.Appointment, BusinessEntities.Appointment>()
                .ForMember(dest => dest.UserID,
                    optns => optns.MapFrom(src => src.User.UserID))
                .ForMember(dest => dest.StartAt,
                    optns => optns.MapFrom(src => src.StartAt.UtcDateTime)) 
                .ForMember(dest => dest.EndAt,
                    optns => optns.MapFrom(src => src.EndAt.UtcDateTime));

            Mapper.CreateMap<ReportParameterSP, ReportParameterMetaData>()
                .ForMember(dest => dest.Id,
                    optns => optns.MapFrom(src => src.ReportParameterId))
                    .ForMember(dest => dest.Name,
                    optns => optns.MapFrom(src => src.ReportParameterName))
                    .ForMember(dest => dest.FieldCaption,
                    optns => optns.MapFrom(src => src.ReportParameterCaption))
                    .ForMember(dest => dest.FieldIndex,
                    optns => optns.MapFrom(src => src.ReportParameterIndex))
                    .ForMember(dest => dest.FieldType,
                    optns => optns.MapFrom(src => src.ReportParameterFieldType))
                    .ForMember(dest => dest.ListOptions,
                    optns => optns.MapFrom(src => src.Data));

            Mapper.CreateMap<ReportParameter, ReportParameterMetaData>()
                .ForMember(dest => dest.Id,
                    optns => optns.MapFrom(src => src.ReportParameterId))
                    .ForMember(dest => dest.Name,
                    optns => optns.MapFrom(src => src.ReportParameterName))
                    .ForMember(dest => dest.FieldCaption,
                    optns => optns.MapFrom(src => src.ReportParameterCaption))
                    .ForMember(dest => dest.FieldIndex,
                    optns => optns.MapFrom(src => src.ReportParameterIndex))
                    .ForMember(dest => dest.FieldType,
                    optns => optns.MapFrom(src => src.ReportParameterFieldType))
                    .ForMember(dest => dest.ListOptions,
                    optns => optns.MapFrom(src => src.SourceOptions));

            Mapper.CreateMap<Data.Model.ListOptions, BusinessEntities.ListOptions>();

        }

        private static string GetFormattedName(Data.Model.Contact contact)
        {
            var sb = new StringBuilder();
            if (contact != null)
            {
                if (!string.IsNullOrEmpty(contact.FirstName))
                    sb.Append(contact.FirstName);

                if (!string.IsNullOrEmpty(contact.MiddleName))
                {
                    if (sb.Length > 0)
                        sb.Append(" ");
                    sb.Append(contact.MiddleName);
                }

                if (!string.IsNullOrEmpty(contact.LastName))
                {
                    if (sb.Length > 0)
                        sb.Append(" ");
                    sb.Append(contact.LastName);
                }
            }
            string response = sb.ToString();
            return response;
        }
    }
}