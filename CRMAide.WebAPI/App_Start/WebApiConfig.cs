﻿using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Http.Cors;

namespace CRMAide.WebAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            config.EnableCors(new EnableCorsAttribute("*", "*", "*"));

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            /// Web API Formatters
            config.Formatters.Clear();
            //config.Formatters.Add( new XmlMediaTypeFormatter());
            //ReferenceLoopHandling.Ignore
            config.Formatters.Add(new JsonMediaTypeFormatter());

            //HttpConfiguration config = GlobalConfiguration.Configuration;

            config.Formatters.JsonFormatter
                        .SerializerSettings
                        .ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
        }
    }
}