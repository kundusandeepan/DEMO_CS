﻿using CRMAide.Data.Interface;
using NHibernate;

namespace CRMAide.WebAPI.App_Start
{
    public class SessionContext : ISessionContext
    {
        public ISession GetCurrentSession()
        {
            return RepositoryModule.Get<ISession>();
        }
    }
}