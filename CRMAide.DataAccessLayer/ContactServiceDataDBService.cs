﻿using System;
using System.Data;
using CRMAide.Data.Interface;
using CRMAide.Data.Model;
using CRMAide.DataAccessLayer.Interface;

namespace CRMAide.DataAccessLayer
{
    public class ContactServiceDataDBService: Repository<ContactService>, IContactServiceDataService
    {
        private readonly IDBHandlerService _dbHandleService;
        public ContactServiceDataDBService(ISessionContext sessionContext, IDBHandlerService dbHandleService) : base(sessionContext)
        {
            _dbHandleService = dbHandleService;
        }

        public int AddService(int contactid, int serviceid, string description)
        {
            return Convert.ToInt32(_dbHandleService.GetScalar("ADD_ServiceForContacts",
                new SPParameter("ContactID", SqlDbType.Int, contactid),
                new SPParameter("ServiceID", SqlDbType.Int, serviceid),
                new SPParameter("Description", SqlDbType.VarChar, description),
                new SPParameter("FiscalYearEndMonth", SqlDbType.Int, DateTime.Now.Year)));
        }
    }
}