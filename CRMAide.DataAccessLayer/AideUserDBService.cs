﻿using CRMAide.Data.Interface;
using CRMAide.Data.Model;

namespace CRMAide.DataAccessLayer
{
    public class AideUserDBService : Repository<AideUser>, IAideUserService
    {
        public AideUserDBService(ISessionContext sessionContext) : base(sessionContext)
        {
        }
    }
}