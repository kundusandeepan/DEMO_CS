﻿using CRMAide.Data.Interface;
using CRMAide.Data.Model;
using CRMAide.DataAccessLayer.Interface;
using System.Data;

namespace CRMAide.DataAccessLayer
{
    public class AssociationDBService : Repository<Association>, IAssociationService
    {
        private readonly IDBHandlerService _dbHandleService;
        public AssociationDBService(ISessionContext sessionContext, IDBHandlerService dbHandleService) : base(sessionContext)
        {
            _dbHandleService = dbHandleService;
        }

        public virtual bool Delete(Association entity)
        {
            _dbHandleService.ExecuteNonQuery("DELETE_Association", 
                new SPParameter("primarycontactid", SqlDbType.Int, entity.PrimaryContact.ContactID),
                new SPParameter("relationcontactid", SqlDbType.Int, entity.SecondayContact.ContactID)
                );
            return true;
        }
    }
}