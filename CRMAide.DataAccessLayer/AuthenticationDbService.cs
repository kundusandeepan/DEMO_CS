﻿using System;
using CRMAide.Data.Interface;
using System.Data;
using CRMAide.DataAccessLayer.Interface;
using NHibernate.Util;

namespace CRMAide.DataAccessLayer
{
    public class AuthenticationDbService : IAuthenticationService
    {
        private readonly ISessionContext _sessionContext;
        private readonly IDBHandlerService _customHandler;

        public AuthenticationDbService(ISessionContext sessionContext, IDBHandlerService customHandler)
        {
            _sessionContext = sessionContext;
            _customHandler = customHandler;
        }

        public int Authenticate(string userName, string password)
        {
            int result = -1;

            result = Convert.ToInt32(_customHandler.GetScalar("GET_LoginStatus",new SPParameter("@LOGIN", SqlDbType.VarChar,userName),new SPParameter("@PWD", SqlDbType.VarChar, password)));

            return result;
        }
        
         
    }
}