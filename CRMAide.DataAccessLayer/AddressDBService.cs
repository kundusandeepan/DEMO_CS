﻿using CRMAide.Data.Interface;
using CRMAide.Data.Model;
using CRMAide.DataAccessLayer.Interface;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace CRMAide.DataAccessLayer
{
    public class AddressDBService : Repository<Address>, IAddressService
    {
        private readonly IDBHandlerService _dbHandleService;

        public AddressDBService(ISessionContext sessionContext, IDBHandlerService dbHandleService) : base(sessionContext)
        {
            _dbHandleService = dbHandleService;
        }

        public IList<Address> Search(Address condition)
        {
            return _dbHandleService.GetRows<Address>("exec SearchAddress :addressid, :street, :city, :zip, :stateid, :countryid, :phone",
                new SPParameter("addressid", SqlDbType.Int, (condition.AddressID == 0) ? (int?)null : condition.AddressID),
                new SPParameter("street", SqlDbType.VarChar, condition.Street1),
                new SPParameter("city", SqlDbType.VarChar, condition.City),
                new SPParameter("zip", SqlDbType.VarChar, condition.Zip),
                new SPParameter("stateid", SqlDbType.Int, (condition.StateID == 0) ? (int?)null : condition.StateID),
                new SPParameter("countryid", SqlDbType.Int, (condition.CountryID == 0) ? (int?)null : condition.CountryID),
                new SPParameter("phone", SqlDbType.VarChar, condition.Phone)
                );
        }

        public IList<string> GetCities(string cityBeginsWith, int stateId, int countryId)
        {
             var data = _dbHandleService.GetRows<AddressCity>("exec SearchCities :city , :stateid, :countryid",
                new SPParameter("city", SqlDbType.VarChar, cityBeginsWith),
                new SPParameter("stateid", SqlDbType.Int, (stateId == 0) ? (int?)null : stateId),
                new SPParameter("countryid", SqlDbType.Int, (countryId == 0) ? (int?)null : countryId),
                new SPParameter("countryid", SqlDbType.Int, (countryId == 0) ? (int?)null : countryId));
            return data.ToList().ConvertAll<string>(t => t.City.Trim());
        }
    }
}