﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;
using NHibernate;
using NHibernate.Context;
using System;
using System.Data.Entity.Design.PluralizationServices;
using System.Globalization;
using System.Reflection;

namespace CRMAide.DataAccessLayer
{
    public enum TargetDBType
    {
        SQLServer,
        MySQL
    }

    public class NHibernateHelper
    {
        private readonly string _connectionString;
        private readonly string _ninjectSessionContextClass;
        private readonly TargetDBType _targetDbType;
        private ISessionFactory _sessionFactory;

        public ISessionFactory SessionFactory
        {
            get
            {
                return _sessionFactory ?? (_sessionFactory = CreateSessionFactory<WebSessionContext>());
            }
        }

        public NHibernateHelper(string connectionString, string ninjectSessionContextClass, TargetDBType targetDbType)
        {
            _connectionString = connectionString;
            _targetDbType = targetDbType;
            _ninjectSessionContextClass = ninjectSessionContextClass;
        }

        private ISessionFactory CreateSessionFactory<T>() where T : ICurrentSessionContext
        {
            switch (_targetDbType)
            {
                case TargetDBType.MySQL:
                    return Fluently.Configure()
                            .Database(MySQLConfiguration.Standard.ConnectionString(_connectionString))
                            .CurrentSessionContext(_ninjectSessionContextClass)
                            .Mappings(m =>
                                m.FluentMappings.AddFromAssembly(Assembly.GetExecutingAssembly())
                                .Conventions.Add<TableNameConvention>()
                            )
                            //.ExposeConfiguration(val =>
                            //{
                            //    val.AppendListeners(ListenerType.PreInsert, new IPreInsertEventListener[] { new PaymasterPreInsertEventListener() });
                            //    val.AppendListeners(ListenerType.PreUpdate, new IPreUpdateEventListener[] { new PaymasterPreUpdateEventListener() });
                            //})
                            //.ExposeConfiguration(cfg => new SchemaExport(cfg).Execute(true, true, false)) //this line creates the database tables in target db
                            //.ExposeConfiguration(cfg => new SchemaUpdate(cfg).Execute(false, true)) //this line creates the database tables in target db
                            //.ExposeConfiguration(x => x.SetProperty("current_session_context_class", "web"))
                            .BuildSessionFactory();
                case TargetDBType.SQLServer:
                    return Fluently.Configure()
                        .Database(MsSqlConfiguration.MsSql2012.ConnectionString(_connectionString))
                        .CurrentSessionContext(_ninjectSessionContextClass)
                        .Mappings(m =>
                            //m.FluentMappings.AddFromAssembly(Assembly.GetExecutingAssembly())
                            m.FluentMappings.AddFromAssemblyOf<AddressDBService>()
                            //.Mappings(m => m.FluentMappings.AddFromAssemblyOf<SqlCommandFactory>())
                            .Conventions.Add<TableNameConvention>()
                        )
                        .BuildSessionFactory();

                default:
                    // for all other types
                    throw new Exception("Type Not Supported");
            }
        }
    }

    public class TableNameConvention : IClassConvention
    {
        public void Apply(IClassInstance instance)
        {
            string typeName = instance.EntityType.Name;
            //var tableName = PluralizationService.CreateService(CultureInfo.CurrentCulture).Pluralize(typeName);
            var tableName = PluralizationService.CreateService(CultureInfo.CurrentCulture).Singularize(typeName);
            var tableNameLowerCase = tableName.ToLower();
            instance.Table(tableNameLowerCase);
        }
    }
}