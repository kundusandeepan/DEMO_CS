using System.Collections.Generic;
using System.Data;

namespace CRMAide.DataAccessLayer.Interface
{
    public interface IDBHandlerService
    {
        void ExecuteNonQuery(string spname, params SPParameter[] spparameters);
        object GetScalar(string spname, params SPParameter[] spparameters);

        IList<T> GetRows<T>(string spname, params SPParameter[] spparameters);
        DataTable GetDataTable(string spname, params SPParameter[] spparameters);
    }
}