﻿using System.Data;

namespace CRMAide.DataAccessLayer.Interface
{
    public class SPParameter
    {
        public string ParamName { get; set; }
        public SqlDbType Type { get; set; }
        public object Value { get; set; }

        public SPParameter(string name, SqlDbType type, object value)
        {
            ParamName = name;
            Type = type;
            Value = value;
        }

//        public SPParameter(string name,  object value)
//        {
//            ParamName = name;
////            Type = type;
//            Value = value;
//        }
    }
}