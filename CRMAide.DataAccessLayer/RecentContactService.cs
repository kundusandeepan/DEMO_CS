﻿using CRMAide.Data.Interface;
using CRMAide.Data.Model;

namespace CRMAide.DataAccessLayer
{
    public class RecentContactService : Repository<RecentContact>, IRecentContactService
    {
        public RecentContactService(ISessionContext sessionContext) : base(sessionContext)
        {
        }
    }
}