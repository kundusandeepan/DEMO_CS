﻿using CRMAide.Data.Interface;
using CRMAide.Data.Model;
using NHibernate.Linq;
using System.Linq;
using System;

namespace CRMAide.DataAccessLayer
{
    public class MasterDataDBService : IMasterDataService
    {
        private readonly ISessionContext _sessionContext;

        public MasterDataDBService(ISessionContext sessionContext)
        {
            _sessionContext = sessionContext;
        }

        public IQueryable<State> GetStates()
        {
            var _session = _sessionContext.GetCurrentSession();
            return _session.Query<State>();
        }
        public IQueryable<Country> GetCountries()
        {
            var _session = _sessionContext.GetCurrentSession();
            return _session.Query<Country>();
        }

        public IQueryable<Questionaire> GetQuestionaires()
        {
            var _session = _sessionContext.GetCurrentSession();
            return _session.Query<Questionaire>();
        }

        public IQueryable<ContactType> GetContactTypes()
        {
            var _session = _sessionContext.GetCurrentSession();
            return _session.Query<ContactType>();
        }

        public IQueryable<ServiceOption> GetServiceOptions()
        {
            var _session = _sessionContext.GetCurrentSession();
            return _session.Query<ServiceOption>();
        }

        public IQueryable<SalutationType> GetSalutations()
        {
            var _session = _sessionContext.GetCurrentSession();
            return _session.Query<SalutationType>();
        }
    }
}