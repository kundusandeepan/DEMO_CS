﻿using CRMAide.Data.Interface;
using CRMAide.Data.Model;
using System.Collections.Generic;
using System.Data;
using CRMAide.DataAccessLayer.Interface;

namespace CRMAide.DataAccessLayer
{
    public class ContactDBService : Repository<Contact>, IContactService
    {
        private readonly IDBHandlerService _dbHandleService;
        public ContactDBService(ISessionContext sessionContext, IDBHandlerService dbHandleService) : base(sessionContext)
        {
            _dbHandleService = dbHandleService;
        }

        public bool DeleteByAddressId(int addressId)
        {
            return Delete(t => t.AddressID == addressId);
        }

        public bool DeleteByContactId(int contactId)
        {
            return Delete(t => t.ContactID == contactId);
        }

        public bool DeleteSubscription(int contactserviceid)
        {
            _dbHandleService.ExecuteNonQuery("DELETE_ContactService", new SPParameter("contactserviceid", SqlDbType.Int,contactserviceid));
            return true;
        }

        //public bool DeleteSubscription(int contactId, int subscriptionid)
        //{
        //    return true;
        //    //invoke _dbHandleService
        //}

        public IList<ContactSearchResult> Search(string query)
        {
            return _dbHandleService.GetRows<ContactSearchResult>("exec GET_SearchResult :SearchText",   new SPParameter("SearchText", SqlDbType.VarChar, query));
        }
    }
}