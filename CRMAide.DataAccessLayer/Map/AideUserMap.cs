﻿using CRMAide.Data.Model;
using FluentNHibernate.Conventions.Helpers;
using FluentNHibernate.Mapping;

namespace CRMAide.DataAccessLayer.Map
{
    public class AideUserMap : ClassMap<AideUser>
    {
        public AideUserMap()
        {
            DefaultLazy.Always();
            Id(x => x.UserID);
            Map(x => x.WorkGroupId).Not.Nullable();
            Map(x => x.Login).Not.Nullable();
            Map(x => x.FirstName);
            Map(x => x.MiddleName);
            Map(x => x.LastName);
            Map(x => x.Email);
            Map(x => x.CustomQuestionaire);
            Map(x => x.QuestionaireAnswer);
        }
    }
}