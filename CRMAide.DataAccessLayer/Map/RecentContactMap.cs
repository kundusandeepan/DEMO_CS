﻿using CRMAide.Data.Model;
using FluentNHibernate.Conventions.Helpers;
using FluentNHibernate.Mapping;

namespace CRMAide.DataAccessLayer.Map
{
    public class RecentContactMap : ClassMap<RecentContact>
    {
        public RecentContactMap()
        {
            DefaultLazy.Always();
            Id(x => x.Id);
            Map(x => x.ContactId);
            Map(x => x.AddressID,"householdid");
            Map(x => x.ContactName);
            Map(x => x.LoggedUserId);
            Map(x => x.AccessDatetime);
        }
    }
}