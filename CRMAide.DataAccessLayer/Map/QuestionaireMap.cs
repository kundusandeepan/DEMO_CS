﻿using CRMAide.Data.Model;
using FluentNHibernate.Conventions.Helpers;
using FluentNHibernate.Mapping;

namespace CRMAide.DataAccessLayer.Map
{
    public class QuestionaireMap : ClassMap<Questionaire>
    {
        public QuestionaireMap()
        {
            DefaultLazy.Always();
            Id(x => x.QuestionaireID);
            Map(x => x.QuestionaireName).Not.Nullable();
        }
    }
}