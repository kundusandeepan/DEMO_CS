﻿using CRMAide.Data.Model;
using FluentNHibernate.Conventions.Helpers;
using FluentNHibernate.Mapping;

namespace CRMAide.DataAccessLayer.Map
{
    public class ReportMap : ClassMap<Report>
    {
        public ReportMap()
        {
            DefaultLazy.Always();
            Id(x => x.ReportId);
            Map(x => x.ReportName);
            Map(x => x.ReportDescription);
            //Map(x => x.ReportCategoryId);
            Map(x => x.ReportIsGraph).Not.Nullable();
            Map(x => x.ReportSpName);

            References(x => x.ReportCategory).Column("ReportCategoryId");
            //.Not.Update()
            //.Not.Insert()
            //.Cascade.All();
        }
    }

    public class ReportCategoryMap : ClassMap<ReportCategory>
    {
        public ReportCategoryMap()
        {
            DefaultLazy.Always();
            Id(x => x.ReportCategoryId);
            Map(x => x.ReportCategoryName);
            Map(x => x.ReportCategoryParentId);
            //References(x => x.ReportCategoryParent).Column("ReportCategoryParentId")
            //    //.Not.Update()
            //    //.Not.Insert()
            //    .Cascade.All();
        }
    }

    public class ReportParameterMap : ClassMap<ReportParameter>
    {
        public ReportParameterMap()
        {
            DefaultLazy.Always();
            Id(x => x.ReportParameterId);
            Map(x => x.ReportParameterName);
            Map(x => x.ReportParameterCaption);
            References(x => x.Report).Column("ReportID")
                //.Not.Update()
                //.Not.Insert()
                .Cascade.All();
            Map(x => x.ReportParameterIndex);
            Map(x => x.ReportParameterFieldType);
            Map(x => x.SourceSP);
            Map(x => x.SourceOptions);
            
        }
    }

    public class ReportParameterSPMap : ClassMap<ReportParameterSP>
    {
        public ReportParameterSPMap()
        {
            DefaultLazy.Always();
            Id(x => x.ReportParameterId);
            Map(x => x.ReportParameterName);
            Map(x => x.ReportParameterCaption);
            Map(x => x.ReportID);
            //            References(x => x.Report).Column("ReportID")
            //                //.Not.Update()
            //                //.Not.Insert()
            //                .Cascade.All();
            Map(x => x.ReportParameterIndex);
            Map(x => x.ReportParameterFieldType);
            Map(x => x.Data);
        }
    }
}