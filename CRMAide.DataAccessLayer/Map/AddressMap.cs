﻿using CRMAide.Data.Model;
using FluentNHibernate.Conventions.Helpers;
using FluentNHibernate.Mapping;

namespace CRMAide.DataAccessLayer.Map
{
    public class AddressMap : ClassMap<Address>
    {
        public AddressMap()
        {
            DefaultLazy.Always();
            Id(x => x.AddressID);
            Map(x => x.Street1);
            Map(x => x.Street2);
            Map(x => x.City);
            Map(x => x.Zip);
            Map(x => x.StateID).Not.Nullable();
            Map(x => x.CountryID).Not.Nullable();
            Map(x => x.Phone);
            Map(x => x.FAX);
            Map(x => x.Latitude);
            Map(x => x.Longitude);

            //ApplyFilter<Address>(" isdeleted = 0");
        }
    }
}