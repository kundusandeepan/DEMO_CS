﻿using CRMAide.Data.Model;
using FluentNHibernate.Conventions.Helpers;
using FluentNHibernate.Mapping;

namespace CRMAide.DataAccessLayer.Map
{
    public class ContactServiceMap : ClassMap<ContactService>
    {
        public ContactServiceMap()
        {
            DefaultLazy.Always();
            Id(x => x.ContactServiceID);
            References(x => x.Contact).Column("ContactID").Not.Nullable();
            References(x => x.Service).Column("ServiceID").Not.Nullable();
            Map(x => x.Description);
            Map(x => x.FiscalYearEndMonth);
            Map(x => x.CreateDate);
            HasMany(x => x.ContactServiceField)
               //.Inverse()
               .Cascade.All();
        }
    }
}