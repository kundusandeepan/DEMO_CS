﻿using CRMAide.Data.Model;
using FluentNHibernate.Conventions.Helpers;
using FluentNHibernate.Mapping;

namespace CRMAide.DataAccessLayer.Map
{
    public class SalutationTypeMap : ClassMap<SalutationType>
    {
        public SalutationTypeMap()
        {
            DefaultLazy.Always();
            Id(x => x.SalutationTypeID).GeneratedBy.Increment();
            Map(x => x.SalutationTypeName);
        }
    }
}