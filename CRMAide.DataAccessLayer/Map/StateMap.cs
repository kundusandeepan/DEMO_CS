﻿using CRMAide.Data.Model;
using FluentNHibernate.Conventions.Helpers;
using FluentNHibernate.Mapping;

namespace CRMAide.DataAccessLayer.Map
{
    public class StateMap : ClassMap<State>
    {
        public StateMap()
        {
            DefaultLazy.Always();
            Id(x => x.StateCountryID);
            Map(x => x.StateID).Not.Nullable();
            Map(x => x.CountryID).Not.Nullable();
            Map(x => x.StateName).Not.Nullable();
            Map(x => x.StateShortName).Not.Nullable();
            
        }
    }
}