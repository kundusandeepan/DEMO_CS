﻿using CRMAide.Data.Model;
using FluentNHibernate.Conventions.Helpers;
using FluentNHibernate.Mapping;

namespace CRMAide.DataAccessLayer.Map
{
    public class CountryMap : ClassMap<Country>
    {
        public CountryMap()
        {
            DefaultLazy.Always();
            Id(x => x.CountryID);
            Map(x => x.CountryName).Not.Nullable();
            Map(x => x.CountryShortName).Not.Nullable();
        }
    }
}