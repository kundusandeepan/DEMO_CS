﻿using CRMAide.Data.Model;
using FluentNHibernate.Conventions.Helpers;
using FluentNHibernate.Mapping;

namespace CRMAide.DataAccessLayer.Map
{
    public class ContactNoteMap : ClassMap<ContactNote>
    {
        public ContactNoteMap()
        {
            DefaultLazy.Always();
            Id(x => x.NoteID).GeneratedBy.Identity();
            Map(x => x.LogDate);
            //Map(x => x.LoggedByUser);
            Map(x => x.Notes);
            References(x => x.Contact).Column("ContactID")
               .Not.Nullable();
            References(x => x.LoggedByUser).Column("LoggedBy")
              .Not.Nullable();
        }
    }
}