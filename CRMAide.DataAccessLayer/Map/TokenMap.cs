﻿using CRMAide.Data.Model;
using FluentNHibernate.Conventions.Helpers;
using FluentNHibernate.Mapping;

namespace CRMAide.DataAccessLayer.Map
{
    public class TokenMap : ClassMap<Token>
    {
        public TokenMap()
        {
            DefaultLazy.Always();
            Id(x => x.Id);
            Map(x => x.UserId);
            Map(x => x.AuthToken);
            Map(x => x.IssuedOn);
            Map(x => x.ExpiresOn);
        }
    }
}