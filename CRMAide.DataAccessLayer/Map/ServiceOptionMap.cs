﻿using CRMAide.Data.Model;
using FluentNHibernate.Conventions.Helpers;
using FluentNHibernate.Mapping;

namespace CRMAide.DataAccessLayer.Map
{
    public class ServiceOptionMap : ClassMap<ServiceOption>
    {
        public ServiceOptionMap()
        {
            DefaultLazy.Always();
            Id(x => x.ServiceID).GeneratedBy.Increment();
            Map(x => x.IsRecurring);
            Map(x => x.ServiceName);
            HasMany(x => x.ServiceFields)
                //.Inverse()
                .Cascade.All();
        }
    }
}