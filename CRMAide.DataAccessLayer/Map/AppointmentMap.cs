﻿using CRMAide.Data.Model;
using FluentNHibernate.Conventions.Helpers;
using FluentNHibernate.Mapping;

namespace CRMAide.DataAccessLayer.Map
{
    public class AppointmentMap : ClassMap<Appointment>
    {
        public AppointmentMap()
        {
            DefaultLazy.Always();
            Id(x => x.Id);//.GeneratedBy.Identity();
            Map(x => x.Title).Not.Nullable();
            Map(x => x.Description).Nullable();
            Map(x => x.StartAt).Not.Nullable();
            Map(x => x.EndAt).Not.Nullable();
            Map(x => x.IsFullDay).Not.Nullable();

            References(x => x.User).Column("UserID").Not.Nullable();
        }
    }
}