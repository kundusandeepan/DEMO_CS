﻿using CRMAide.Data.Model;
using FluentNHibernate.Conventions.Helpers;
using FluentNHibernate.Mapping;

namespace CRMAide.DataAccessLayer.Map
{
    public class ContactTypeMap : ClassMap<ContactType>
    {
        public ContactTypeMap()
        {
            DefaultLazy.Always();
            Id(x => x.ContactTypeID);
            Map(x => x.ContactName).Not.Nullable();
        }
    }
}