﻿using CRMAide.Data.Model;
using FluentNHibernate.Conventions.Helpers;
using FluentNHibernate.Mapping;

namespace CRMAide.DataAccessLayer.Map
{
    public class ContactMap : ClassMap<Contact>
    {
        public ContactMap()
        {
            DefaultLazy.Always();
            Id(x => x.ContactID);
            Map(x => x.SalutationID).Not.Nullable();
            Map(x => x.SIN);
            Map(x => x.FirstName);
            Map(x => x.MiddleName);
            Map(x => x.LastName);
            Map(x => x.IsMale).Not.Nullable();
            Map(x => x.Email);
            Map(x => x.DateOfBirth);
            Map(x => x.AddressID);
            Map(x => x.MobilePhone);
            Map(x => x.StatusInCountry);
            Map(x => x.SinceInCountry);
            Map(x => x.Description);
            HasMany(x => x.ServicesSubscription)
                //.Inverse()
                .Cascade.All();
            HasMany(x => x.Notes)
                //.Inverse()
                .Cascade.All();
            HasMany(x => x.Association)
                //.Inverse()
                .Cascade.All();
        }
    }
}