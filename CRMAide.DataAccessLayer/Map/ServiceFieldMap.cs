﻿using CRMAide.Data.Model;
using FluentNHibernate.Conventions.Helpers;
using FluentNHibernate.Mapping;

namespace CRMAide.DataAccessLayer.Map
{
    public class ServiceFieldMap : ClassMap<ServiceField>
    {
        public ServiceFieldMap()
        {
            DefaultLazy.Always();
            Id(x => x.ServiceFieldID).GeneratedBy.Identity();
            Map(x => x.FieldIndex);
            Map(x => x.FieldCaption);
            Map(x => x.FieldType);
            Map(x => x.ListOptions);
            Map(x => x.DefaultValue);
            Map(x => x.MaxValue);
            Map(x => x.MinValue);
            References(x => x.ServiceOption).Column("ServiceID")
                .Not.Nullable();
            //.LazyLoad();
        }
    }
}