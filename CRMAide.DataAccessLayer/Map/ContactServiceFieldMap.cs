﻿using CRMAide.Data.Model;
using FluentNHibernate.Conventions.Helpers;
using FluentNHibernate.Mapping;

namespace CRMAide.DataAccessLayer.Map
{
    public class ContactServiceFieldMap : ClassMap<ContactServiceField>
    {
        public ContactServiceFieldMap()
        {
            DefaultLazy.Always();
            Id(x => x.ContactServiceFieldID);
            References(x => x.ContactService).Column("ContactServiceID").Not.Nullable();
            References(x => x.ServiceField).Column("ServiceFieldID").Not.Nullable();
            Map(x => x.ServiceFieldValue);
            Map(x => x.PeriodNo);
            Map(x => x.Periodicity);
            Map(x => x.Year);
            Map(x => x.CreateDate);
        }
    }
}