﻿using CRMAide.Data.Model;
using FluentNHibernate.Conventions.Helpers;
using FluentNHibernate.Mapping;

namespace CRMAide.DataAccessLayer.Map
{
    public class AssociationMap : ClassMap<Association>
    {
        public AssociationMap()
        {
            DefaultLazy.Always();
            CompositeId()
              .KeyReference(x => x.PrimaryContact, "MainContactID")
              .KeyReference(x => x.SecondayContact, "AssociateContactID");

            /*
            Explanation :http://stackoverflow.com/a/7997225
            when used id, use KeyProperty
            when used class, use KeyReference

            CompositeId()
              .KeyProperty(x => x.MainContactID, "MainContactID")
              .KeyProperty(x => x.AssociateContactID, "AssociateContactID");
            */

            References(x => x.PrimaryContact).Column("MainContactID")
                .Not.Update()
                .Not.Insert()
                .Cascade.All();
            References(x => x.SecondayContact).Column("AssociateContactID")
                .Not.Update()
                .Not.Insert()
                .Cascade.All();

            //            Map(x => x.PrimaryContact).Column("MainContactID");
            //            Map(x => x.SecondayContact).Column("AssociateContactID");


            References(x => x.ContactType).Column("ContactTypeID").Not.Nullable();
            Map(x => x.IsAssociateDependent);
        }
    }
}