using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CRMAide.CommonInterfaces;
using System.Runtime.Caching;

namespace CRMAide.DataAccessLayer.Caching
{
    public class RuntimeMemoryCache : ILoadInMemoryCache
    {
        private readonly MemoryCache _memoryCache;
        public RuntimeMemoryCache()
        {
            _memoryCache = MemoryCache.Default;
        }

        public IQueryable<T> GetAll<T>() where T : class
        {
            var list = new List<T>();
            var cacheEnumerator = (IDictionaryEnumerator)((IEnumerable)_memoryCache).GetEnumerator();
            while (cacheEnumerator.MoveNext())
            {
                var value = cacheEnumerator.Value as T;
                if(value != null)
                    list.Add(value);
            }
            return list.AsQueryable();
        }

        public T GetItem<T>(string cacheKey) where T : class
        {
            return _memoryCache.Get(cacheKey) as T;
        }

        public bool SetItem<T>(string cacheKey, T cacheValue) where T : class
        {
            if (_memoryCache.Contains(cacheKey))
            {
                _memoryCache.Remove(cacheKey);
            }

            var item = new CacheItem(cacheKey, cacheValue);
            var policy = new CacheItemPolicy(){
                Priority = CacheItemPriority.NotRemovable,
            };
            return _memoryCache.Add(item, policy);
            
        }

        public bool RemoveItem<T>(string cacheKey, T cacheValue) where T : class
        {
            if (_memoryCache.Contains(cacheKey))
            {
                _memoryCache.Remove(cacheKey);
                return true;
            }
            return false;
        }
    }
}
