﻿using CRMAide.Data.Interface;
using CRMAide.DataAccessLayer.Interface;
using NHibernate;
using NHibernate.Transform;
using NHibernate.Type;
using NHibernate.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace CRMAide.DataAccessLayer
{
    public class DBHandlerService : IDBHandlerService
    {
        private readonly ISessionContext _sessionContext;

        public DBHandlerService(ISessionContext sessionContext)
        {
            _sessionContext = sessionContext;
        }

        public void ExecuteNonQuery(string spname, params SPParameter[] spparameters)
        {
            var session = _sessionContext.GetCurrentSession();

            using (var command = session.Connection.CreateCommand())
            {
                session.Transaction.Enlist(command);

                command.CommandText = spname;
                command.CommandType = CommandType.StoredProcedure;

                foreach (var eachParam in spparameters)
                {
                    var parameterPassword = new SqlParameter(eachParam.ParamName, eachParam.Type);
                    parameterPassword.Value = eachParam.Value;
                    command.Parameters.Add(parameterPassword);
                }

                command.ExecuteNonQuery();
            }
        }

        public object GetScalar(string spname, params SPParameter[] spparameters)
        {
            var session = _sessionContext.GetCurrentSession();

            using (var command = session.Connection.CreateCommand())
            {
                session.Transaction.Enlist(command);

                command.CommandText = spname;
                command.CommandType = CommandType.StoredProcedure;

                foreach (var eachParam in spparameters)
                {
                    var parameterPassword = new SqlParameter(eachParam.ParamName, eachParam.Type);
                    parameterPassword.Value = eachParam.Value;
                    command.Parameters.Add(parameterPassword);
                }

                return command.ExecuteScalar();
            }
        }

        public IList<T> GetRows<T>(string spname, params SPParameter[] spparameters)
        {
            var session = _sessionContext.GetCurrentSession();
            var isqlquery = session.CreateSQLQuery(spname);
            foreach (var parameter in spparameters)
            {
                isqlquery.SetParameter(parameter.ParamName, parameter.Value, GetNHibernateType(parameter.Type));
            }
            //return isqlquery.SetResultTransformer(Transformers.AliasToBean(typeof(Address))).List<T>();
            return isqlquery.SetResultTransformer(Transformers.AliasToBean(typeof(T))).List<T>();
        }

        public DataTable GetDataTable(string spname, params SPParameter[] spparameters)
        {
            var session = _sessionContext.GetCurrentSession();
            var isqlquery = session.CreateSQLQuery(spname);
            foreach (var parameter in spparameters)
            {
                isqlquery.SetParameter(parameter.ParamName, parameter.Value, GetNHibernateType(parameter.Type));
            }
            //return isqlquery.SetResultTransformer(Transformers.AliasToBean(typeof(Address))).List<T>();
            var transformedquery = isqlquery.SetResultTransformer(new DataTableResultTransformer());
            var xx = transformedquery.List();
            var yy = xx.First();
            var zz = yy as DataTable;
            return zz;
        }

        private IType GetNHibernateType(SqlDbType sqlDBType)
        {
            switch (sqlDBType)
            {
                case SqlDbType.Int: return NHibernateUtil.Int32;
                case SqlDbType.BigInt: return NHibernateUtil.Int64;
                case SqlDbType.SmallInt: return NHibernateUtil.Int32;
                case SqlDbType.TinyInt: return NHibernateUtil.Int16;
                case SqlDbType.Char: return NHibernateUtil.Character;
                case SqlDbType.NText:
                case SqlDbType.VarChar: return NHibernateUtil.AnsiString;
                case SqlDbType.Bit: return NHibernateUtil.Boolean;
                case SqlDbType.Binary: return NHibernateUtil.Binary;
                case SqlDbType.VarBinary: return NHibernateUtil.BinaryBlob;
                case SqlDbType.Time: return NHibernateUtil.Time;
                case SqlDbType.Timestamp: return NHibernateUtil.Timestamp;
                case SqlDbType.SmallDateTime: return NHibernateUtil.DateTime;
                case SqlDbType.Date: return NHibernateUtil.Date;
                case SqlDbType.DateTime: return NHibernateUtil.DateTime;
                case SqlDbType.DateTime2: return NHibernateUtil.DateTime2;
                case SqlDbType.Decimal: return NHibernateUtil.Decimal;
                case SqlDbType.Float: return NHibernateUtil.Decimal;
                case SqlDbType.Xml: return NHibernateUtil.XmlDoc;
                default:
                    throw new Exception(string.Format("Type {0}, not implemented", sqlDBType.ToString()));
            }
        }
    }
}