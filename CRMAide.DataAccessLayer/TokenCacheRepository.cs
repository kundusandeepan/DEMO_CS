﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using CRMAide.CommonInterfaces;
using CRMAide.Data.Interface;
using CRMAide.Data.Model;
using NHibernate.Util;

namespace CRMAide.DataAccessLayer
{
    public class TokenCacheRepository : ITokenRepository
    {
        private readonly ILoadInMemoryCache _memoryCache;
        public TokenCacheRepository(ILoadInMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;
        }
        public IQueryable<Token> All()
        {
            return _memoryCache.GetAll<Token>();
        }

        public Token FindBy(Func<Token, bool> @where)
        {
            return All().Where(@where).FirstOrDefault();
        }

        public IQueryable<Token> FilterBy(Func<Token, bool> @where)
        {
            return All().Where(@where).AsQueryable();
        }

        public bool Add(Token entity)
        {
            return _memoryCache.SetItem(entity.AuthToken, entity);
        }

        public bool Add(IEnumerable<Token> items)
        {
            var retVal=true;
            items.ForEach(t =>{
                if (!Add(t))
                    retVal = false;
            });
            return retVal;
        }

        public bool Update(Token entity)
        {
            return _memoryCache.SetItem(entity.AuthToken, entity);
        }

        public bool Delete(Token entity)
        {
            return _memoryCache.RemoveItem(entity.AuthToken, entity);
        }

        public bool Delete(IEnumerable<Token> entities)
        {
            var retVal = true;
            entities.ForEach(t => {
                if (!Delete(t))
                    retVal = false;
            });
            return retVal;
        }

        public bool Delete(Expression<Func<Token, bool>> expression)
        {
            var retVal = true;
            All().Where(expression).ForEach(t => { 
                if (!Delete(t))
                    retVal = false;
            });
            return retVal;
        }
    }
}