﻿using CRMAide.Data.Interface;
using NHibernate.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace CRMAide.DataAccessLayer
{
    public abstract class Repository<T> : IIntKeyedRepository<T> where T : class
    {
        private readonly ISessionContext _sessionContext;

        public Repository(ISessionContext sessionContext)
        {
            _sessionContext = sessionContext;
        }

        #region IRepository<T> Members

        public virtual bool Add(T entity)
        {
            var _session = _sessionContext.GetCurrentSession();
            using (var transaction = _session.BeginTransaction())
            {
                _session.Save(entity);
                transaction.Commit();
            }
            _session.Flush();
            return true;
        }

        public virtual bool Add(IEnumerable<T> items)
        {
            var _session = _sessionContext.GetCurrentSession();
            using (var transaction = _session.BeginTransaction())
            {
                foreach (T item in items)
                {
                    _session.Save(item);
                }
                transaction.Commit();
            }
            _session.Flush();
            return true;
        }

        public virtual bool Update(T entity)
        {
            var _session = _sessionContext.GetCurrentSession();
            using (var transaction = _session.BeginTransaction())
            {
                //_session.Update(entity);
                _session.SaveOrUpdate(entity);
                transaction.Commit();
            }
            _session.Flush();
            return true;
        }

        public virtual bool Delete(T entity)
        {
            var _session = _sessionContext.GetCurrentSession();
            using (var transaction = _session.BeginTransaction())
            {
                _session.Delete(entity);
                transaction.Commit();
            }
            _session.Flush();
            return true;
        }

        public virtual bool Delete(Expression<System.Func<T, bool>> expression)
        {
            var _session = _sessionContext.GetCurrentSession();
            using (var transaction = _session.BeginTransaction())
            {
                var objects = All().Where(expression).AsQueryable();
                foreach (var obj in objects)
                {
                    _session.Delete(obj);
                }
                transaction.Commit();
            }
            _session.Flush();
            return true;
        }

        public virtual bool Delete(IEnumerable<T> entities)
        {
            var _session = _sessionContext.GetCurrentSession();
            using (var transaction = _session.BeginTransaction())
            {
                foreach (T entity in entities)
                {
                    _session.Delete(entity);
                }
                transaction.Commit();
            }
            _session.Flush();
            return true;
        }

        #endregion IRepository<T> Members

        #region IIntKeyedRepository<T> Members

        public virtual T FindBy(int id)
        {
            var _session = _sessionContext.GetCurrentSession();
            //using (var transaction = _session.BeginTransaction())
            //{
            return _session.Get<T>(id);
            //}
        }

        #endregion IIntKeyedRepository<T> Members

        #region IReadOnlyRepository<T> Members

        public virtual IQueryable<T> All()
        {
            var _session = _sessionContext.GetCurrentSession();
            //using (var transaction = _session.BeginTransaction())
            //{
            return _session.Query<T>();
            //}
        }

        //public virtual T FindBy(Expression<System.Func<T, bool>> expression)
        //{
        //    return FilterBy(expression).Single();
        //}

        //public virtual IQueryable<T> FilterBy(Expression<System.Func<T, bool>> expression)
        //{
        //    return All().Where(expression).AsQueryable();
        //}

        public virtual T FindBy(Func<T, Boolean> where)
        {
            return FilterBy(where).SingleOrDefault();
        }

        public virtual IQueryable<T> FilterBy(Func<T, Boolean> where)
        {
            return All().Where(where).AsQueryable();
        }

        #endregion IReadOnlyRepository<T> Members

        
    }
}