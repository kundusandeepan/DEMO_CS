﻿using CRMAide.Data.Interface;
using CRMAide.Data.Model;

namespace CRMAide.DataAccessLayer
{
    public class ServiceOptionDBService : Repository<ServiceOption>, IServiceOptionService
    {
        public ServiceOptionDBService(ISessionContext sessionContext) : base(sessionContext)
        {
        }
    }
}