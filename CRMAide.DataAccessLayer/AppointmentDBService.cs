﻿using CRMAide.Data.Interface;
using CRMAide.Data.Model;

namespace CRMAide.DataAccessLayer
{
    public class AppointmentDBService : Repository<Appointment>, IAppointmentService
    {
        public AppointmentDBService(ISessionContext sessionContext) : base(sessionContext)
        {
        }
    }
}