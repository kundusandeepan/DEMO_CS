﻿
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using CRMAide.Data.Interface;
using CRMAide.Data.Model;
using CRMAide.DataAccessLayer.Interface;

namespace CRMAide.DataAccessLayer
{
    public class ReportCategoryService : Repository<ReportCategory>, IReportCategoryService
    {
        public ReportCategoryService(ISessionContext sessionContext) : base(sessionContext)
        {
        }
    }

    public class ReportService : Repository<Report>, IReportService
    {
        private readonly IDBHandlerService _dbHandleService;
        private readonly IReportParameterService _reportParameterService;

        public ReportService(ISessionContext sessionContext, IDBHandlerService dbHandleService, IReportParameterService reportParameterService) : base(sessionContext)
        {
            _dbHandleService = dbHandleService;
            _reportParameterService = reportParameterService;
        }

        public DataTable GetTabularReport(int ReportId, Dictionary<int, string> FieldValuePair)
        {
            var report = FindBy(ReportId);
            if (report == null) return null;
            
            StringBuilder paramterNamesBuilder = new StringBuilder();
            List<SPParameter> parameters = new List<SPParameter>();
            var reportparameters = _reportParameterService.FilterBy(t => t.Report.ReportId == ReportId);
            if (reportparameters!=null)
            {
                foreach (var fieldValue in FieldValuePair)
                {
                    var rp = reportparameters.FirstOrDefault(t => t.ReportParameterId == fieldValue.Key);
                    if (rp != null)
                    {
                        var sp = new SPParameter(rp.ReportParameterName, SqlDbType.Int,  fieldValue.Value);
                        paramterNamesBuilder.Append(string.Format(":{0}", rp.ReportParameterName));


                        parameters.Add(sp);
                    }
                }
            }
            string paramterNames = paramterNamesBuilder.ToString();
            var spcCmd = string.Format("exec {0} {1}", report.ReportSpName, paramterNames);

            //identify reportname from reportid;
            //int ReportId
            return _dbHandleService.GetDataTable(spcCmd, parameters.ToArray());
        }
    }

    public class ReportParameterService : Repository<ReportParameter>, IReportParameterService
    {
        private readonly IDBHandlerService _dbHandleService;
        public ReportParameterService(ISessionContext sessionContext, IDBHandlerService dbHandleService) : base(sessionContext)
        {
            _dbHandleService = dbHandleService;
        }
        
        public IList<ListOptions> GetSourceSPData(int reportParamterId)
        {
            var report = FindBy(t => t.ReportParameterId == reportParamterId);
            if (report != null && !string.IsNullOrWhiteSpace(report.SourceSP))
            {
                return _dbHandleService.GetRows<ListOptions>( string.Format("exec {0}", report.SourceSP));
            }
            return null;
        }

        public IList<ListOptions> GetSourceSPData(string spname)
        {
            if (!string.IsNullOrWhiteSpace(spname))
            {
                return _dbHandleService.GetRows<ListOptions>(string.Format("exec {0}", spname));
            }
            return null;
        }
    }
}