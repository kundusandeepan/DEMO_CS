﻿using System;
using System.Collections.Generic;

namespace CRMAide.BusinessEntities
{
    public class ContactBase
    {
        public virtual string FirstName { get; set; }
        public virtual string MiddleName { get; set; }
        public virtual string LastName { get; set; }
        public virtual int ContactID { get; set; }
        public virtual int AddressID { get; set; }
    }

    public class ContactShortDTO : ContactBase
    {
//        public string Street1 { get; set; }
//        public string Street2 { get; set; }
//        public string City { get; set; }
    }
    public class Contact: ContactBase
    {
        public virtual int SalutationID { get; set; }
        public virtual string SIN { get; set; }
        public virtual bool IsMale { get; set; }
        public virtual string Email { get; set; }
        public virtual DateTime? DateOfBirth { get; set; }
        public virtual string MobilePhone { get; set; }
        public virtual int StatusInCountry { get; set; }
        public virtual int SinceInCountry { get; set; }
        public virtual IList<ServicesSubscription> ServicesSubscription { get; set; }
        //public virtual IList<Note> Notes { get; set; }
        public virtual string Description { get; set; }

        public virtual IList<Association> Association { get; set; }
    }

    
}