﻿using System;

namespace CRMAide.BusinessEntities
{
    public class ContactServiceField
    {
        public virtual int ContactServiceFieldID { get; set; }
        public virtual int ContactServiceID { get; set; }
        public virtual int ServiceFieldID { get; set; }
        public virtual string ServiceFieldValue { get; set; }
        public virtual int PeriodNo { get; set; }
        public virtual int Periodicity { get; set; }
        public virtual int Year { get; set; }
        public virtual DateTime CreateDate { get; set; }
    }
}