﻿using System;

namespace CRMAide.BusinessEntities
{
    public class ServicesSubscription
    {
        public virtual int ContactServiceID { get; set; }
        public virtual int ServiceID { get; set; }
        public virtual string ServiceName { get; set; }
        public virtual string Description { get; set; }
        public virtual DateTime CreateDate { get; set; }
    }
}