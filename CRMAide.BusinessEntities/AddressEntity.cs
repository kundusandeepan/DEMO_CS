﻿namespace CRMAide.BusinessEntities
{
    public class AddressEntity
    {
        public int AddressID { get; set; }
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public StateEntity State { get; set; }
        public CountryEntity Country { get; set; }
        public string Phone { get; set; }
        public string FAX { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}