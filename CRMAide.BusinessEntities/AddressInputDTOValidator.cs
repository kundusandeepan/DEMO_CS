﻿using FluentValidation;

namespace CRMAide.BusinessEntities
{
    public class AddressInputDTOValidator : AbstractValidator<AddressInputDTO>
    {
        public AddressInputDTOValidator()
        {
            RuleFor(input => input.Street1)
                //.Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty()
                .WithMessage("{PropertyName} cannot be empty");

            RuleFor(input => input.City)
                .NotEmpty()
                .WithMessage("{PropertyName} cannot be empty");

            RuleFor(input => input.Zip)
                .NotEmpty()
                .WithMessage("{PropertyName} cannot be empty");
            //@Length validation

            RuleFor(input => input.StateId)
                .NotEmpty()
                .WithMessage("{PropertyName} cannot be empty");

            RuleFor(input => input.CountryId)
                .NotEmpty()
                .WithMessage("{PropertyName} cannot be empty");
            

            //.Must( )


            //IRuleBuilderOptions(input=>input.street1)
        }

    }
}