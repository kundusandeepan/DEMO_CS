using System.Collections.Generic;

namespace CRMAide.BusinessEntities
{
    public class TreeNode
    {
        public TreeNode()
        {
            Children = new List<TreeNode>();
            NodeType = "category";//NodeType.Category;
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        //        public NodeType NodeType { get; protected set; }
        public string NodeType { get; protected set; }
        public List<TreeNode> Children { get; set; }
    }

    public class ReportTreeNode: TreeNode
    {
        
        public ReportTreeNode()
        {
            //            NodeType = NodeType.Report;
            SetNodeType();
        }

        private bool _isGraph = false;

        private void SetNodeType()
        {
            NodeType = _isGraph ? "graph" : "report";
        }
        public bool IsGraph { get { return _isGraph; } set { _isGraph = value; SetNodeType(); } }
    }
}