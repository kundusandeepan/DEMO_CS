namespace CRMAide.BusinessEntities
{
    public class CitiesAutoCompleteDTO
    {
        public int CountryId { get; set; }
        public int StateId { get; set; }
        public string Query { get; set; }
    }
}