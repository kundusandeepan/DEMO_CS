﻿using System;

namespace CRMAide.BusinessEntities
{
    public class Note
    {
        public virtual int NoteID { get; set; }
        public virtual int ContactID { get; set; }
        public virtual string Notes { get; set; }
        public virtual string LoggedBy { get; set; }   //Assuiming shouldnt it be contact ID of the person creating it ?!!
        public virtual DateTime LogDate { get; set; }
    }
}