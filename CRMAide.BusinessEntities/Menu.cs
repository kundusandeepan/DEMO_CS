﻿using System.Collections.Generic;

namespace CRMAide.BusinessEntities
{
    public class Menu
    {
        public int Id { get; set; }
        public string DisplayName { get; set; }
        public string Tag { get; set; }
        public string Link { get; set; }
        public string Target { get; set; }
        public List<Menu> SubMenus { get; set; }
    }
}