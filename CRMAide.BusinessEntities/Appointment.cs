﻿using System;

namespace CRMAide.BusinessEntities
{
    public class AppointmentBase
    {
        public virtual int Id { get; set; }
        public virtual string Title { get; set; }
        public virtual string Description { get; set; }
        public virtual bool IsFullDay { get; set; }
    }
    public class AppointmentInputDTO: AppointmentBase
    {
        public virtual DateTime Start { get; set; }
        public virtual DateTime End { get; set; }
    }

    public class Appointment : AppointmentBase
    {
        public virtual int UserID { get; set; }

        public virtual DateTime StartAt { get; set; }
        public virtual DateTime EndAt { get; set; }
    }
}