﻿namespace CRMAide.BusinessEntities
{
    public class CountryEntity
    {
        public virtual int CountryID { get; set; }
        public virtual string CountryName { get; set; }
        public virtual string CountryShortName { get; set; }
    }
}