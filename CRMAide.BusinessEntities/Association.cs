﻿namespace CRMAide.BusinessEntities
{
    public class Association
    {
        public virtual int PrimaryContactID { get; set; }
        public virtual int RelationContactID { get; set; }
        public virtual int RelationAddressID { get; set; }
        public virtual string RelationContactName { get; set; }
        public virtual int ContactTypeID { get; set; }
        public virtual bool IsAssociateDependent { get; set; }
    }


    public class AssociationDTO
    {
        public virtual int PrimaryContactID { get; set; }
        public virtual int RelationContactID { get; set; }
        public virtual int ContactTypeID { get; set; }
        public virtual bool IsAssociateDependent { get; set; }
    }
}