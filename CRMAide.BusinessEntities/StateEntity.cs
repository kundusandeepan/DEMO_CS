﻿namespace CRMAide.BusinessEntities
{
    public class StateEntity
    {
        public virtual int StateID { get; set; }
        public virtual int CountryID { get; set; }
        public virtual int StateName { get; set; }
        public virtual int StateShortName { get; set; }
    }
}