﻿using System.Collections.Generic;

namespace CRMAide.BusinessEntities
{
    public class ContactServiceDTO
    {
        public int SubscriptionId { get; set; }
        public Dictionary<int, string> FieldValuePair { get; set; }  
    }
}