﻿using System.Collections.Generic;

namespace CRMAide.BusinessEntities
{
    public class ReportParameterMetaData
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string FieldCaption { get; set; }
        public virtual int ReportID { get; set; }
        public virtual int FieldIndex { get; set; }
        public virtual int FieldType { get; set; }
        public virtual string ListOptions { get; set; }
        public virtual IList<ListOptions> ListOptions2 { get; set; }
    }

    public class ListOptions
    {
        public string ID { get; set; }
        public string DisplayName { get; set; }
    }
}