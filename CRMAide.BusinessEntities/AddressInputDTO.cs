﻿using FluentValidation.Attributes;
using System.ComponentModel.DataAnnotations;

namespace CRMAide.BusinessEntities
{
    public struct MessageId
    {
        public const string MissingRequiredInput = "Error Missing Required Input";
    }

    [Validator(typeof(AddressInputDTOValidator))]
    public class AddressInputDTO
    {
        [Required(ErrorMessage = MessageId.MissingRequiredInput)]
        public string Street1 { get; set; }

        public string Street2 { get; set; }
        public string City { get; set; }

        [StringLength(7)]
        [Display(Name = "Postal Code / Zip")]
        public string Zip { get; set; }

        public int StateId { get; set; }
        public int CountryId { get; set; }
        public string Phone { get; set; }
        public string FAX { get; set; }
    }
}