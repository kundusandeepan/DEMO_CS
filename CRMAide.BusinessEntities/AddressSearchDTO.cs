﻿namespace CRMAide.BusinessEntities
{
    public class AddressSearchDTO
    {
        public int AddressId { get; set; }
        public string Street1 { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public int StateId { get; set; }
        public int CountryId { get; set; }
        public string Phone { get; set; }
    }
}