﻿using AutoMapper;
using CRMAide.BusinessEntities;
using CRMAide.BusinessServices.Interface;
using CRMAide.Data.Interface;
using CRMAide.Data.Model;
using System;
using System.Configuration;
using System.Linq;

namespace CRMAide.BusinessServices
{
    public class TokenService :  ITokenService
    {
        #region Private member variables.

        private readonly ITokenRepository _tokenRepository;
        private readonly IAuthenticationService _authenticationService;
        private readonly double expiryDuration;
        private readonly IAideUserService _userService;

        #endregion Private member variables.

        #region Public constructor.

        /// <summary>
        /// Public constructor.
        /// </summary>
        public TokenService(ITokenRepository tokenRepository, IAuthenticationService authenticationService, IAideUserService userService)
        {
            _tokenRepository = tokenRepository;
            _authenticationService = authenticationService;
            expiryDuration = Convert.ToDouble(ConfigurationManager.AppSettings["AuthTokenExpiry"]);
            _userService = userService;
        }

        #endregion Public constructor.

        #region Public member methods.

        /// <summary>
        ///  Function to generate unique token with expiry against the provided userId.
        ///  Also add a record in database for generated token.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public TokenEntity GenerateToken(int userId)
        {
            string token = Guid.NewGuid().ToString();
            DateTime issuedOn = DateTime.Now;
            DateTime expiredOn = DateTime.Now.AddSeconds(expiryDuration);
            var tokendomain = new Token
            {
                UserId = userId,
                AuthToken = token,
                IssuedOn = issuedOn,
                ExpiresOn = expiredOn,
            };
            _tokenRepository.Add(tokendomain);
            return Mapper.Map<TokenEntity>(tokendomain);
        }

        /// <summary>
        /// Method to fetch TokenEntity from database
        /// </summary>
        /// <param name="tokenId"></param>
        /// <returns></returns>
        public TokenEntity Get(string tokenId)
        {
            var token = _tokenRepository.FindBy(t => t.AuthToken == tokenId && t.ExpiresOn > DateTime.Now);
            return token == null ? null : Mapper.Map<TokenEntity>(token);
        }

        /// <summary>
        /// Method to validate token against expiry and existence in database.
        /// </summary>
        /// <param name="tokenId"></param>
        /// <returns></returns>
        public bool ValidateToken(string tokenId)
        {
            var token = _tokenRepository.FindBy(t => t.AuthToken == tokenId && t.ExpiresOn > DateTime.Now);
            if (token != null && !(DateTime.Now > token.ExpiresOn))
            {
                token.ExpiresOn = DateTime.Now.AddSeconds(expiryDuration);
                _tokenRepository.Update(token);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Method to kill the provided token id.
        /// </summary>
        /// <param name="tokenId">true for successful delete</param>
        public bool Kill(string tokenId)
        {
            _tokenRepository.Delete(x => x.AuthToken == tokenId);
            var isNotDeleted = _tokenRepository.FilterBy(x => x.AuthToken == tokenId).Any();
            return !isNotDeleted;
        }

        /// <summary>
        /// Delete tokens for the specific deleted user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>true for successful delete</returns>
        public bool DeleteByUserId(int userId)
        {
            _tokenRepository.Delete(x => x.UserId == userId);
            var isNotDeleted = _tokenRepository.FilterBy(x => x.UserId == userId).Any();
            return !isNotDeleted;
        }

        public TokenEntity AuthenticateUser(string username, string password)
        {
            var userid = _authenticationService.Authenticate(username, password);
            if (userid > 0)
            {
                var tokenEntity = GenerateToken(userid);
                var aideUser = _userService.FindBy(t => t.UserID == userid);
                tokenEntity.AvatarName = aideUser.Login;
                //tokenEntity.UserName=
                return tokenEntity;
            }
            //else user
            return null;
        }

        #endregion Public member methods.
    }
}