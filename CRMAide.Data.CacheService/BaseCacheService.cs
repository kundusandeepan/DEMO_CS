using CRMAide.CommonInterfaces;

namespace CRMAide.Data.CacheService
{
    public abstract class BaseCacheService<T> : BaseService<T> where T : class
    {
        protected readonly ILoadInMemoryCache _cache;

        public BaseCacheService(T next, ILoadInMemoryCache cache)
            : base(next)
        {
            _cache = cache;
        }
    }
}