namespace CRMAide.Data.CacheService
{
    public class BaseService<T> where T : class
    {
        protected T Next;

        public BaseService(T next)
        {
            Next = next;
        }
    }
}