namespace CRMAide.Data.CacheService
{
    internal enum CacheKeys
    {
        State,
        Country,
        Questionaire,
        ContactType,
        ServiceOption,
        SalutationType
    }
}