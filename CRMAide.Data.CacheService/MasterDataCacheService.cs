using System.Linq;
using CRMAide.CommonInterfaces;
using CRMAide.Data.Interface;
using CRMAide.Data.Model;

namespace CRMAide.Data.CacheService
{
    public class MasterDataCacheService : BaseCacheService<IMasterDataService>, IMasterDataService
    {
        public MasterDataCacheService(IMasterDataService next, ILoadInMemoryCache cache) : base(next, cache)
        {
        }

        public IQueryable<State> GetStates()
        {
            var items = _cache.GetItem<IQueryable<State>>(CacheKeys.State.ToString());
            if (items == null)
            {
                items = Next.GetStates();
                _cache.SetItem<IQueryable<State>>(CacheKeys.State.ToString(), items);
            }
            return items;
        }

        public IQueryable<Country> GetCountries()
        {
            var items = _cache.GetItem<IQueryable<Country>>(CacheKeys.Country.ToString());
            if (items == null)
            {
                items = Next.GetCountries();
                _cache.SetItem<IQueryable<Country>>(CacheKeys.Country.ToString(), items);
            }
            return items;
        }

        public IQueryable<Questionaire> GetQuestionaires()
        {
            var items = _cache.GetItem<IQueryable<Questionaire>>(CacheKeys.Questionaire.ToString());
            if (items == null)
            {
                items = Next.GetQuestionaires();
                _cache.SetItem<IQueryable<Questionaire>>(CacheKeys.Questionaire.ToString(), items);
            }
            return items;
        }

        public IQueryable<ContactType> GetContactTypes()
        {
            var items = _cache.GetItem<IQueryable<ContactType>>(CacheKeys.ContactType.ToString());
            if (items == null)
            {
                items = Next.GetContactTypes();
                _cache.SetItem<IQueryable<ContactType>>(CacheKeys.ContactType.ToString(), items);
            }
            return items;
        }

        public IQueryable<ServiceOption> GetServiceOptions()
        {
            var items = _cache.GetItem<IQueryable<ServiceOption>>(CacheKeys.ServiceOption.ToString());
            if (items == null)
            {
                items = Next.GetServiceOptions();
                _cache.SetItem<IQueryable<ServiceOption>>(CacheKeys.ServiceOption.ToString(), items);
            }
            return items;
        }

        public IQueryable<SalutationType> GetSalutations()
        {
            var items = _cache.GetItem<IQueryable<SalutationType>>(CacheKeys.SalutationType.ToString());
            if (items == null)
            {
                items = Next.GetSalutations();
                _cache.SetItem<IQueryable<SalutationType>>(CacheKeys.SalutationType.ToString(), items);
            }
            return items;
        }
    }
}