﻿using System.Collections.Generic;

namespace CRMAide.Data.Model
{
    public class ServiceOption
    {
        public virtual int ServiceID { get; set; }

        public virtual string ServiceName { get; set; }

        public virtual bool IsRecurring { get; set; }

        public virtual IList<ServiceField> ServiceFields { get; set; }
    }
}