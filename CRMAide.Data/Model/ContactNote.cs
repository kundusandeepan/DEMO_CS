﻿using System;

namespace CRMAide.Data.Model
{
    public class ContactNote
    {
        public virtual int NoteID { get; set; }
        //public virtual int ContactID { get; set; }
        public virtual Contact Contact { get; set; }
        public virtual string Notes { get; set; }
        public virtual AideUser LoggedByUser { get; set; }
        public virtual DateTime LogDate { get; set; }
    }
}