﻿using System;
using System.Collections.Generic;

namespace CRMAide.Data.Model
{
    public class ContactService
    {
        public virtual int ContactServiceID { get; set; }

        //public virtual int ContactID { get; set; }
        public virtual Contact Contact { get; set; }

        //public virtual int ServiceID { get; set; }
        public virtual ServiceOption Service { get; set; }

        public virtual string Description { get; set; }

        public virtual int FiscalYearEndMonth { get; set; }

        public virtual DateTime CreateDate { get; set; }

        public virtual IList<ContactServiceField> ContactServiceField { get; set; }
    }
}