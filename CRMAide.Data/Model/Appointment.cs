﻿using System;

namespace CRMAide.Data.Model
{
    public class Appointment
    {
        public virtual int Id { get; set; }
        public virtual AideUser User { get; set; }
        public virtual string Title { get; set; }
        public virtual string Description { get; set; }
        public virtual DateTimeOffset StartAt { get; set; }
        public virtual DateTimeOffset EndAt { get; set; }
        public virtual bool IsFullDay { get; set; }
    }
}