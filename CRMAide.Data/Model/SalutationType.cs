﻿namespace CRMAide.Data.Model
{
    public class SalutationType
    {
        public virtual int SalutationTypeID { get; set; }

        public virtual string SalutationTypeName { get; set; }
    }
}