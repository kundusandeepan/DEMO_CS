﻿namespace CRMAide.Data.Model
{
    public class Module
    {
        public string Name { get; set; }
        public int Id { get; set; }
    }
}