﻿namespace CRMAide.Data.Model
{
    public class AddressCity
    {
        public virtual string City { get; set; }
    }

    public class Address: AddressCity
    {
        public virtual int AddressID { get; set; }
        public virtual string Street1 { get; set; }
        public virtual string Street2 { get; set; }
        public virtual string Zip { get; set; }
        public virtual int StateID { get; set; }
        public virtual int CountryID { get; set; }
        public virtual string Phone { get; set; }
        public virtual string FAX { get; set; }
        public virtual double Latitude { get; set; }
        public virtual double Longitude { get; set; }
    }
}