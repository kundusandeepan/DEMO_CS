﻿namespace CRMAide.Data.Model
{
    public class AideUser
    {
        public virtual int UserID { get; set; }
        public virtual int WorkGroupId { get; set; }
        public virtual string Login { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string MiddleName { get; set; }
        public virtual string LastName { get; set; }
        public virtual string Email { get; set; }
        public virtual string CustomQuestionaire { get; set; }
        public virtual string QuestionaireAnswer { get; set; }
    }
}