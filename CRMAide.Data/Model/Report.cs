﻿namespace CRMAide.Data.Model
{

    public class ReportCategory
    {
        public virtual int ReportCategoryId { get; set; }
        public virtual string ReportCategoryName { get; set; }
        public virtual int ReportCategoryParentId { get; set; }
        //public virtual ReportCategory ReportCategoryParent { get; set; }
    }
    public class Report
    {
        public virtual int ReportId { get; set; }
        public virtual string ReportName { get; set; }
        public virtual string ReportDescription { get; set; }
        //public virtual int ReportCategoryId { get; set; }
        public virtual ReportCategory ReportCategory { get; set; }
        public virtual bool ReportIsGraph { get; set; }
        public virtual string ReportSpName { get; set; }
    }

    public class ReportParameterBase
    {
        public virtual int ReportParameterId { get; set; }
        public virtual string ReportParameterName { get; set; }
        public virtual string ReportParameterCaption { get; set; }
        public virtual int ReportParameterIndex { get; set; }
        public virtual int ReportParameterFieldType { get; set; }
    }

    public class ReportParameter : ReportParameterBase
    {
        public virtual Report Report { get; set; }
        public virtual string SourceSP { get; set; }
        public virtual string SourceOptions { get; set; }
    }

    public class ReportParameterSP : ReportParameterBase
    {
        public virtual int ReportID { get; set; }
        public virtual string Data { get; set; }
    }

    public class ListOptions
    {
        public int ID { get; set; }
        public string DisplayName { get; set; }
    }
}