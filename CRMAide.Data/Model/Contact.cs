﻿using System;
using System.Collections.Generic;

namespace CRMAide.Data.Model
{
    public class Contact
    {
        public virtual int ContactID { get; set; }
        public virtual int SalutationID { get; set; }
        public virtual string SIN { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string MiddleName { get; set; }
        public virtual string LastName { get; set; }
        public virtual bool IsMale { get; set; }
        public virtual string Email { get; set; }
        public virtual DateTime? DateOfBirth { get; set; }
        public virtual int AddressID { get; set; }
        public virtual string MobilePhone { get; set; }
        public virtual int StatusInCountry { get; set; }
        public virtual int SinceInCountry { get; set; }
        public virtual IList<ContactService> ServicesSubscription { get; set; }
        public virtual IList<ContactNote> Notes { get; set; }
        public virtual string Description { get; set; }

        public virtual IList<Association> Association { get; set; }
    }
    
}