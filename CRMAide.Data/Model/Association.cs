﻿namespace CRMAide.Data.Model
{
    public class Association
    {

        public virtual Contact PrimaryContact { get; set; }
        public virtual Contact SecondayContact { get; set; }
        public virtual ContactType ContactType { get; set; }
        public virtual bool IsAssociateDependent { get; set; }

        public override bool Equals(object obj)
        {
            var other = obj as Association;

            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            
            return this.PrimaryContact == other.PrimaryContact &&
            this.SecondayContact == other.SecondayContact ;
        }
        public override int GetHashCode()
        {
            unchecked
            {
                int hash = GetType().GetHashCode();

                hash = (hash * 31) ^ PrimaryContact.GetHashCode();
                hash = (hash * 31) ^ SecondayContact.GetHashCode();
                
                return hash;
            }
        }
    }
}