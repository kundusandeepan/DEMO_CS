﻿namespace CRMAide.Data.Model
{
    public class Privilege
    {
        public virtual int WorkGroupID { get; set; }
        public virtual int ModuleID { get; set; }
    }
}