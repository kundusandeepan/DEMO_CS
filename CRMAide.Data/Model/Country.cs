﻿namespace CRMAide.Data.Model
{
    public class Country
    {
        public virtual int CountryID { get; set; }
        public virtual string CountryName { get; set; }
        public virtual string CountryShortName { get; set; }
    }
}