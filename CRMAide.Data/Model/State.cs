﻿using System;

namespace CRMAide.Data.Model
{
    public class State
    {
        public virtual int StateCountryID { get; set; }
        public virtual Int16 StateID { get; set; }
        public virtual int CountryID { get; set; }
        public virtual string StateName { get; set; }
        public virtual string StateShortName { get; set; }

    }
}