﻿using System;

namespace CRMAide.Data.Model
{
    [Serializable]
    public class ModelBase
    {
        private DateTime _addedDate;
        private DateTime? _modifiedDate;

        public virtual DateTime AddedDateTime
        {
            get
            {
                if (default(DateTime) == _addedDate)
                {
                    return DateTime.Now;
                }
                else
                    return _addedDate;
            }
            set
            {
                _addedDate = value;
            }
        }

        public virtual DateTime? ModifiedDateTime
        {
            get
            {
                if (_modifiedDate == null)
                {
                    return DateTime.Now;
                }
                else
                    return _modifiedDate;
            }
            set
            {
                _modifiedDate = value;
            }
        }
    }
}