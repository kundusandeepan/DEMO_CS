﻿using System;

namespace CRMAide.Data.Model
{
    public class RecentContact
    {
        public virtual int Id { get; set; }
        public virtual int AddressID { get; set; }
        public virtual int ContactId { get; set; }
        public virtual string ContactName { get; set; }
        public virtual int LoggedUserId { get; set; }
        public virtual DateTime AccessDatetime { get; set; }
    }
}