﻿namespace CRMAide.Data.Model
{
    public class ContactType
    {
        public virtual int ContactTypeID { get; set; }
        public virtual string ContactName { get; set; }
    }
}