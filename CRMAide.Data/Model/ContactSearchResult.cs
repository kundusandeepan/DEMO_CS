using System;

namespace CRMAide.Data.Model
{
    public class ContactSearchResult
    {
        public int AddressID { get; set; }
        public int ContactID { get; set; }
        public string SalutationTypeName { get; set; }
        public string SIN { get; set; }
        public string FirstName{ get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string Email { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string MobilePhone { get; set; }
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string CountryName{ get; set; }
        public string CountryShortName { get; set; }
        public string StateName { get; set; }
        public string StateShortName { get; set; }
    }
}