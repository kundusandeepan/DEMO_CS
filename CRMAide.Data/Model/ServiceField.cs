﻿namespace CRMAide.Data.Model
{
    public class ServiceField
    {
        public virtual int ServiceFieldID { get; set; }

        //public virtual int ServiceID { get; set; }
        public virtual ServiceOption ServiceOption { get; set; }

        public virtual int FieldIndex { get; set; }
        public virtual string FieldCaption { get; set; }
        public virtual int FieldType { get; set; }
        public virtual string ListOptions { get; set; }
        public virtual string DefaultValue { get; set; }
        public virtual string MaxValue { get; set; }
        public virtual string MinValue { get; set; }
    }
}