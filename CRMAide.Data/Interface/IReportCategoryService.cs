﻿using System.Collections.Generic;
using System.Data;
using System.Security.Cryptography.X509Certificates;
using CRMAide.Data.Model;

namespace CRMAide.Data.Interface
{
    public interface IReportCategoryService : IRepository<ReportCategory>
    {
    }
    public interface IReportService : IRepository<Report>
    {
        DataTable GetTabularReport(int ReportId, Dictionary<int, string> FieldValuePair);
    }
    public interface IReportParameterService : IRepository<ReportParameter>
    {
//        IList<ReportParameterSP> GetParameters(int reportId);
        IList<ListOptions> GetSourceSPData(int reportParamterId);
        IList<ListOptions> GetSourceSPData(string spname);
    }
}