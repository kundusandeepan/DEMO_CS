﻿using CRMAide.Data.Model;

namespace CRMAide.Data.Interface
{
    public interface ITokenRepository : IRepository<Token>
    {
    }
}