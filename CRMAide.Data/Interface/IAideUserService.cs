﻿using CRMAide.Data.Model;

namespace CRMAide.Data.Interface
{
    public interface IAideUserService : IRepository<AideUser>
    {
    }
}