﻿using System;

namespace CRMAide.Data.Interface
{
    public interface IGuidKeyedReadOnlyRepository<TEntity> : IReadOnlyRepository<TEntity> where TEntity : class
    {
        TEntity FindBy(Guid id);
    }
}