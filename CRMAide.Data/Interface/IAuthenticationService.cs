﻿namespace CRMAide.Data.Interface
{
    public interface IAuthenticationService
    {
        /// <summary>
        /// Returns Authenticated user's ID or Error Code
        /// </summary>
        /// <param name="userName">username/login to authenticate</param>
        /// <param name="password">password in plain text</param>
        /// <returns></returns>
        int Authenticate(string userName, string password);
    }
}