namespace CRMAide.Data.Interface
{
    public interface IIntKeyedRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        TEntity FindBy(int id);
    }
}