﻿using CRMAide.Data.Model;

namespace CRMAide.Data.Interface
{
    public interface IAppointmentService : IRepository<Appointment>
    {
    }
}