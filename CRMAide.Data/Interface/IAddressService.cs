﻿using CRMAide.Data.Model;
using System.Collections.Generic;

namespace CRMAide.Data.Interface
{
    public interface IAddressService : IRepository<Address>, ISearchable<Address>
    {
        IList<string> GetCities(string cityBeginsWith, int stateId, int countryId);
    }
}