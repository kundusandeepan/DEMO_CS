﻿using System.Collections.Generic;
using CRMAide.Data.Model;

namespace CRMAide.Data.Interface
{
    public interface IContactService : IRepository<Contact>
    {
        bool DeleteByAddressId(int addressId);
        bool DeleteByContactId(int contactId);
        //bool DeleteSubscription(int contactId,int subscriptionid);
        bool DeleteSubscription(int contactserviceid);
        IList<ContactSearchResult> Search(string query);
    }
}