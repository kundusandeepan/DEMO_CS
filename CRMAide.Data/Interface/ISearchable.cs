﻿using System.Collections.Generic;

namespace CRMAide.Data.Interface
{
    public interface ISearchable<T>
    {
        IList<T> Search(T condition);
    }
}