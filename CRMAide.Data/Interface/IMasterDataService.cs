﻿using CRMAide.Data.Model;
using System.Linq;

namespace CRMAide.Data.Interface
{
    public interface IMasterDataService
    {
        IQueryable<State> GetStates();

        IQueryable<Country> GetCountries();

        IQueryable<Questionaire> GetQuestionaires();

        IQueryable<ContactType> GetContactTypes();

        IQueryable<ServiceOption> GetServiceOptions();

        IQueryable<SalutationType> GetSalutations(); 
    }
}