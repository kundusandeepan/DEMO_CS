﻿using CRMAide.Data.Model;

namespace CRMAide.Data.Interface
{
    public interface IContactServiceDataService : IRepository<ContactService>
    {
        int AddService(int contactid, int serviceid, string description);

    }
}