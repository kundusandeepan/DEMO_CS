﻿using CRMAide.Data.Model;

namespace CRMAide.Data.Interface
{
    public interface IAssociationService : IRepository<Association>
    {
    }
}