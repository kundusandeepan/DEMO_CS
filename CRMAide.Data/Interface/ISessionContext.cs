using NHibernate;

namespace CRMAide.Data.Interface
{
    public interface ISessionContext
    {
        ISession GetCurrentSession();
    }
}