﻿using CRMAide.Data.Model;

namespace CRMAide.Data.Interface
{
    public interface IRecentContactService : IRepository<RecentContact>
    {
    }
}