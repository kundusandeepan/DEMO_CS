﻿using System;
using System.Linq;

namespace CRMAide.Data.Interface
{
    public interface IReadOnlyRepository<TEntity> where TEntity : class
    {
        IQueryable<TEntity> All();

        TEntity FindBy(Func<TEntity, bool> where);

        IQueryable<TEntity> FilterBy(Func<TEntity, Boolean> where);
    }
}