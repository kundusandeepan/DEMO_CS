﻿using CRMAide.Data.Model;

namespace CRMAide.Data.Interface
{
    public interface IServiceOptionService : IRepository<ServiceOption>
    {
    }
}