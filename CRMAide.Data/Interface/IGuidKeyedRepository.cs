using System;

namespace CRMAide.Data.Interface
{
    public interface IGuidKeyedRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        TEntity FindBy(Guid id);
    }
}