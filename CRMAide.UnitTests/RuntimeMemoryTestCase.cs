﻿using System;
using System.Linq;
using CRMAide.CommonInterfaces;
using CRMAide.Data.Interface;
using CRMAide.Data.Model;
using CRMAide.DataAccessLayer;
using CRMAide.DataAccessLayer.Caching;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject;

namespace CRMAide.UnitTests
{
    [TestClass]
    public class RuntimeMemoryTestCase
    {
        private IKernel kernel;
        private ITokenRepository tokenRepository;

        [TestInitialize()]
        public void Initialize()
        {
            kernel = new StandardKernel();
            kernel.Bind<ILoadInMemoryCache>().To<RuntimeMemoryCache>();
            kernel.Bind<ITokenRepository>().To<TokenCacheRepository>();
            tokenRepository = kernel.Get<ITokenRepository>();
        }

        [TestMethod]
        public void Token_Add_update()
        {
            var token = new Token()
            {
                Id = 1,
                UserId = 12,
                IssuedOn = DateTime.Now,
                ExpiresOn = DateTime.Now.AddDays(2),
                AuthToken = Guid.NewGuid().ToString()
            };
            Assert.IsTrue(tokenRepository.Add(token), "token adding failed");

            var fetchedtoken = tokenRepository.FindBy(t => t.AuthToken == token.AuthToken);

            Assert.IsTrue(token.Id == fetchedtoken.Id);
            Assert.IsTrue(token.UserId == fetchedtoken.UserId);
            Assert.IsTrue(token.IssuedOn == fetchedtoken.IssuedOn);
            Assert.IsTrue(token.ExpiresOn == fetchedtoken.ExpiresOn);
            Assert.IsTrue(token == fetchedtoken);

            var dt = fetchedtoken.ExpiresOn.AddDays(2);
            fetchedtoken.ExpiresOn = dt;

            tokenRepository.Update(fetchedtoken);
            var refetchedtoken = tokenRepository.FindBy(t => t.AuthToken == token.AuthToken);
            
            Assert.IsTrue(dt == refetchedtoken.ExpiresOn);

        }


        [TestMethod]
        public void MultiTokenAdd()
        {
            var token1 = new Token()
            {
                Id = 1,
                UserId = 12,
                IssuedOn = DateTime.Now,
                ExpiresOn = DateTime.Now.AddDays(2),
                AuthToken = Guid.NewGuid().ToString()
            };
            Assert.IsTrue(tokenRepository.Add(token1), "token1 adding failed");

            var token2 = new Token()
            {
                Id = 2,
                UserId = 23,
                IssuedOn = DateTime.Now,
                ExpiresOn = DateTime.Now.AddDays(2),
                AuthToken = Guid.NewGuid().ToString()
            };
            Assert.IsTrue(tokenRepository.Add(token2), "token2 adding failed");

            var fetchedtoken2 = tokenRepository.FindBy(t => t.AuthToken == token2.AuthToken);
            Assert.IsTrue(token2 == fetchedtoken2);

            var fetchedtoken1 = tokenRepository.FindBy(t => t.AuthToken == token1.AuthToken);
            Assert.IsTrue(token1 == fetchedtoken1);
        }


        [TestMethod]
        public void MultiTokenAdd_SingleDelete()
        {
            var issueddate = DateTime.Now;
            var token1 = new Token()
            {
                Id = 1,
                UserId = 12,
                IssuedOn = issueddate,
                ExpiresOn = DateTime.Now.AddDays(2),
                AuthToken = Guid.NewGuid().ToString()
            };
            Assert.IsTrue(tokenRepository.Add(token1), "token1 adding failed");

            var token2 = new Token()
            {
                Id = 2,
                UserId = 23,
                IssuedOn = issueddate,
                ExpiresOn = DateTime.Now.AddDays(2),
                AuthToken = Guid.NewGuid().ToString()
            };
            Assert.IsTrue(tokenRepository.Add(token2), "token2 adding failed");


            var token3 = new Token()
            {
                Id = 3,
                UserId = 31,
                IssuedOn = DateTime.Now,
                ExpiresOn = DateTime.Now.AddDays(2),
                AuthToken = Guid.NewGuid().ToString()
            };
            Assert.IsTrue(tokenRepository.Add(token3), "token3 adding failed");

            var fetchedtoken2 = tokenRepository.FindBy(t => t.AuthToken == token2.AuthToken);
            Assert.IsTrue(token2 == fetchedtoken2);

            var fetchedtoken1 = tokenRepository.FindBy(t => t.AuthToken == token1.AuthToken);
            Assert.IsTrue(token1 == fetchedtoken1);


            var fetchedtoken3 = tokenRepository.FindBy(t => t.AuthToken == token3.AuthToken);
            Assert.IsTrue(token3 == fetchedtoken3);

            Assert.IsTrue(tokenRepository.All().Count() == 3, "count is incorrect");

            Assert.IsTrue(tokenRepository.Delete(t => t.IssuedOn == issueddate), "deleting token(s) failed");

            Assert.IsTrue(tokenRepository.All().Count() == 1, "count is incorrect");

            Assert.IsTrue(tokenRepository.Delete(token3), "deleting token failed");
            Assert.IsTrue(tokenRepository.All().Count() == 0, "count is incorrect");
        }
    }
}