﻿using System.Collections.Generic;
using System.Linq;
using CRMAide.BusinessEntities;
using CRMAide.Data.Interface;
using CRMAide.Data.Model;
using CRMAide.UnitTests.Plumbing;
using FluentNHibernate.Conventions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject;
using Association = CRMAide.Data.Model.Association;
using Contact = CRMAide.Data.Model.Contact;

namespace CRMAide.UnitTests
{
    [TestClass]
    public class DbUnitTest
    {
        private IKernel kernel;
        private IAideUserService aideUserService;
        private IAuthenticationService authenticationService;
        private IAddressService addressService;
        private IServiceOptionService serviceOptionService;
        private IContactService contactservice;
        private IMasterDataService masterDataService;
        private IAssociationService associationService;

        private IReportService _reportservice;
        private IReportCategoryService _reportCategoryService;

        [TestInitialize()]
        public void Initialize()
        {
            kernel = new StandardKernel();
            kernel.Settings.AllowNullInjection = true;
            kernel.Load(new RepositoryModule());
            aideUserService = kernel.Get<IAideUserService>();
            authenticationService = kernel.Get<IAuthenticationService>();
            addressService = kernel.Get<IAddressService>();
            masterDataService = kernel.Get<IMasterDataService>();
            serviceOptionService = kernel.Get<IServiceOptionService>();
            contactservice = kernel.Get<IContactService>();
            associationService = kernel.Get<IAssociationService>();
            //Bind<IAssociationService>().To<AssociationDBService>();
            _reportservice = kernel.Get<IReportService>();
            _reportCategoryService = kernel.Get<IReportCategoryService>();
        }

        [TestMethod]
        public void CheckAuthenticationStoredProc_valid_Authentication()
        {
            Assert.AreEqual(authenticationService.Authenticate("AdminUser01", "password"), 3);
        }

        [TestMethod]
        public void CheckAuthenticationStoredProc_invalid_Authentication()
        {
            Assert.AreEqual(authenticationService.Authenticate("AdminUser01X", "password"), -3, "username password is not correct");
        }

        [TestMethod]
        public void GET_AideUser_Count_GE_0()
        {
            var allusers = aideUserService.All();
            Assert.IsTrue(allusers.Count() > 0);
        }


        [TestMethod]
        public void GET_Address_Count_GE_0()
        {
            var allrecords= addressService.All();
            Assert.IsTrue(allrecords.Count() > 0);
        }


        [TestMethod]
        public void GET_Country_Count_GE_0()
        {
            var allrecords = masterDataService.GetCountries();
            Assert.IsTrue(allrecords.Count() > 0);

            var allrecords2 = masterDataService.GetCountries();
            Assert.IsTrue(allrecords2.Count() > 0);

            Assert.IsTrue(allrecords.Count() == allrecords2.Count(), "Country list data from cache and db are not the same");
        }

        [TestMethod]
        public void GET_State_Count_GE_0()
        {
            var allrecords = masterDataService.GetStates();
            Assert.IsTrue(allrecords.Count() > 0);

            var allrecords2 = masterDataService.GetStates();
            Assert.IsTrue(allrecords2.Count() > 0);

            Assert.IsTrue(allrecords.Count() == allrecords2.Count(),"State list data from cache and db are not the same");
        }

        [TestMethod]
        public void GET_Questionaire_Count_GE_0()
        {
            var allrecords = masterDataService.GetQuestionaires();
            Assert.IsTrue(allrecords.Count() > 0);

            var allrecords2 = masterDataService.GetQuestionaires();
            Assert.IsTrue(allrecords2.Count() > 0);

            Assert.IsTrue(allrecords.Count() == allrecords2.Count(), "Questionaire List data from cache and db are not the same");
        }

        [TestMethod]
        public void GET_ContactType_Count_EQ_0()
        {
            var allrecords = masterDataService.GetContactTypes();
            Assert.IsTrue(allrecords.Count()  == 0);

            var allrecords2 = masterDataService.GetContactTypes();
            Assert.IsTrue(allrecords2.Count() == 0);

            Assert.IsTrue(allrecords.Count() == allrecords2.Count(), "GetContactType List data from cache and db are not the same");
        }


        [TestMethod]
        public void GET_Contact_Count_GE_0()
        {
            var allrecords = contactservice.All();
            Assert.IsTrue(allrecords.Count() > 0);
        }

        [TestMethod]
        public void ContactTests_ADD_Find_Delete()
        {
            var contact = new Contact
            {
                AddressID = 1,
                DateOfBirth = null,
                FirstName = "Harekramba",
                LastName = "Hatta",
                MiddleName = "Halum",
                Email = "Harekramba.Hatta@Halum.com",
                IsMale = true,
                MobilePhone = "6471234567",
                SIN = "123456789",
                SalutationID = 1,
                SinceInCountry = 1234,
                StatusInCountry = 1,
            };
            var IsAdded = contactservice.Add(contact);
            Assert.IsTrue(IsAdded, "Could not add");
            var newcontactid = contact.ContactID;


            var newcontact = contactservice.FindBy(t => t.ContactID == newcontactid);

            var isrecordsame = contact.Equals(newcontact);
            Assert.IsTrue(isrecordsame, "Could not find the inserted record");

            var isdeleted = contactservice.DeleteByContactId(newcontactid);
            Assert.IsTrue(isdeleted, "Could not delete by ID");
        }

        [TestMethod]
        public void AddressSearch_NoCondition_test()
        {
            var address = new Address()
            {
                //AddressID = 1,
                //City = "brampton"
            };
            var result = addressService.Search(address);
            Assert.IsTrue(result.Count() > 0);
        }

        [TestMethod]
        public void Address_CityAutocomplete()
        {
            var result = addressService.GetCities("mi",2,1);
            Assert.IsTrue(result.Count() > 0);
        }

        [TestMethod]
        public void AddressSearch_one_record_test()
        {
            var address = new Address()
            {
                AddressID = 1,
                //City = "brampton"
            };
            var result = addressService.Search(address);
            Assert.IsTrue(result.Count() ==1);
        }

        [TestMethod]
        public void ServiceOptions_GetAll()
        {
            var result = serviceOptionService.All();
            
            Assert.IsTrue(result.Count() > 0);
        }

        [TestMethod]
        public void ServiceOptions_GetByServiceId()
        {
            int serviceId = 1;
            var result = serviceOptionService.FindBy(t=>t.ServiceID == serviceId);

            Assert.IsTrue(result !=null);
        }

        [TestMethod]
        public void ServiceOptionsFields_ADD()
        {
            //TODO:Pending for ServiceField.ServiceID Issue

            //var serviceOption = new ServiceOption()
            //{
            //    IsRecurring   = true,
            //    ServiceName = "ABCD",
            //    ServiceFields = new List<ServiceField>()
            //    {
            //        new ServiceField() { FieldIndex = 1, FieldCaption = "a1", FieldType = 1, DefaultValue = "AAA", ListOptions = "a|b|c|d", MaxValue = "10",MinValue = "1"},
            //        new ServiceField() { FieldIndex = 2, FieldCaption = "a2", FieldType = 1, DefaultValue = "BBB", ListOptions = "P|Q|R|S", MaxValue = "100",MinValue = "1"},
            //    }
            //};
            //var result = serviceOptionService.Add(serviceOption);
            //Assert.IsTrue(result,"could not insert Service Option");
        }

        [TestMethod]
        public void Test_Association_Add_find_delete()
        {
            var primaryContact = contactservice.FindBy(t => t.ContactID == 822);
            var secondaryContact = contactservice.FindBy(t => t.ContactID == 1177);
            var contacttype = masterDataService.GetContactTypes().ToList().Find(t => t.ContactTypeID == 3);
            var association = new Association()
            {
                PrimaryContact = primaryContact,
                SecondayContact = secondaryContact,
                ContactType = contacttype,
                IsAssociateDependent = false,
            };
            var IsAdded = associationService.Add(association);
            
            Assert.IsTrue(IsAdded, "Could not add Association");
            
            var newcontact = associationService.FindBy(t => t.PrimaryContact == primaryContact && t.SecondayContact == secondaryContact);

            var isrecordsame = association.Equals(newcontact);
            Assert.IsTrue(isrecordsame, "Could not find the inserted record");

            var isdeleted = associationService.Delete(association);
            Assert.IsTrue(isdeleted, "Could not delete association");
        }

        [TestMethod]
        public void Test_Contact_And_SubscriptionServices()
        {
            var primaryContact  = contactservice.FindBy(t => t.ContactID == 822);

            Assert.IsTrue(primaryContact!=null);
        }

        [TestMethod]
        public void Debug_testbed()
        {
            var data = _reportservice.All();
            Assert.IsTrue(data != null);

            var data2 = _reportCategoryService.All();
            Assert.IsTrue(data2 != null);

            //find top level
            var data3 = BuildTree(0);//roots are ZERO
            
            Assert.IsTrue(data3 != null);
        }

        #region "Build Tree"
        private IEnumerable<TreeNode> BuildTree(int startReportCategoryId)
        {
            var nodeList = new List<TreeNode>();
            foreach (var category in GetReportCategoryByParentId(startReportCategoryId))
            {
                var tn = new TreeNode
                {
                    Name = category.ReportCategoryName,
                    Id = category.ReportCategoryId,
                };
                foreach (var report in GetReportByCategoryId(category.ReportCategoryId))
                {
                    var report_tn = new ReportTreeNode
                    {
                        Id = report.ReportId,
                        Name = report.ReportName,
                        Description = report.ReportDescription,
                        IsGraph = report.ReportIsGraph
                    };
                    tn.Children.Add(report_tn);
                }
                if(category.ReportCategoryId>0)
                    tn.Children.AddRange(BuildTree(category.ReportCategoryId));
                nodeList.Add(tn);
            }
            return nodeList;
        }

        private IEnumerable<Report> GetReportByCategoryId(int categoryid)
        {
            return _reportservice.FilterBy(t => t.ReportCategory.ReportCategoryId == categoryid).ToList();
        }

        private IEnumerable<ReportCategory> GetReportCategoryByParentId(int parentId)
        {
            return _reportCategoryService.FilterBy(t => t.ReportCategoryParentId == parentId).ToList();
        }
        #endregion
    }
}