using System.Configuration;
using CRMAide.Data.Interface;
using CRMAide.DataAccessLayer;
using NHibernate;
using NHibernate.Context;
using Ninject;
using Ninject.Activation;
using Ninject.Modules;
using CRMAide.CommonInterfaces;
using CRMAide.Data.CacheService;
using CRMAide.DataAccessLayer.Caching;
using CRMAide.DataAccessLayer.Interface;

namespace CRMAide.UnitTests.Plumbing
{
    public class RepositoryModule : NinjectModule
    {
        private static IKernel _kernel;

        public override void Load()
        {
            _kernel = Kernel;
            string connectionString = ConfigurationManager.ConnectionStrings["defaultconnection"].ConnectionString;
            string ninjectSessionContextClass = ConfigurationManager.AppSettings["NinjectSessionContextClass"];

            NHibernateHelper helper = new NHibernateHelper(connectionString, ninjectSessionContextClass, TargetDBType.SQLServer);
            Bind<ISessionFactory>().ToConstant(helper.SessionFactory).InSingletonScope();
            Bind<ISessionContext>().To<SessionContext>();
            Bind<ISession>().ToMethod(CreateSession).InSingletonScope();
            Bind<ITokenRepository>().To<TokenCacheRepository>();
            Bind<IAuthenticationService>().To<AuthenticationDbService>();

            Bind<IDBHandlerService>().To<DBHandlerService>();

            Bind<IAideUserService>().To<AideUserDBService>();
            Bind<IAddressService>().To<AddressDBService>();
            Bind<IAppointmentService>().To<AppointmentDBService>();
            Bind<IServiceOptionService>().To<ServiceOptionDBService>();
            Bind<IContactService>().To<ContactDBService>();
            Bind<IContactServiceDataService>().To<ContactServiceDataDBService>();

            Bind<IAssociationService>().To<AssociationDBService>();

            Bind<IReportService>().To<ReportService>();
            Bind<IReportCategoryService>().To<ReportCategoryService>();
            Bind<IReportParameterService>().To<ReportParameterService>();

            //Bind<IMasterDataService>().To<MasterDataDBService>();

            Bind<IMasterDataService>().To<MasterDataCacheService>().InSingletonScope();
            Bind<IMasterDataService>().To<MasterDataDBService>().WhenInjectedExactlyInto(typeof(MasterDataCacheService)).InSingletonScope();
            Bind<IMasterDataService>().ToConstant<IMasterDataService>(null).WhenInjectedExactlyInto(typeof(MasterDataDBService)).InSingletonScope();

            Bind<ILoadInMemoryCache>().To<RuntimeMemoryCache>();
        }

        public static T Get<T>()
        {
            return _kernel.Get<T>();
        }

        public static T Get<T>(string name)
        {
            return _kernel.Get<T>(name);
        }

        private ISession CreateSession(IContext context)
        {
            var sessionFactory = context.Kernel.Get<ISessionFactory>();
            if (!CurrentSessionContext.HasBind(sessionFactory))
            {
                var session = sessionFactory.OpenSession();
                session.FlushMode = FlushMode.Never;
                CurrentSessionContext.Bind(session);
            }
            return sessionFactory.GetCurrentSession();
        }
    }
}