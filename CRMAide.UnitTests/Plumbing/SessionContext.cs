﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRMAide.Data.Interface;
using NHibernate;

namespace CRMAide.UnitTests.Plumbing
{
    public class SessionContext : ISessionContext
    {
        public ISession GetCurrentSession()
        {
            return RepositoryModule.Get<ISession>();
        }
    }
}
