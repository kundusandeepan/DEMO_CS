using System.Linq;

namespace CRMAide.CommonInterfaces
{
    public interface ILoadInMemoryCache
    {
        IQueryable<T> GetAll<T>() where T : class;

        T GetItem<T>(string cacheKey) where T : class;

        bool SetItem<T>(string cacheKey, T cacheValue) where T : class;

        bool RemoveItem<T>(string cacheKey, T cacheValue) where T : class;
    }
}